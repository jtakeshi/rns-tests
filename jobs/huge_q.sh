#!/bin/bash

#$ -M jtakeshi@nd.edu
#$ -m abe
#$ -q *@@jung
#$ -pe smp 4
#$ -N inc_q
#

MULT=1
MOD=2
GAMMA=4
MTILDE=8
#Run from root directory
EXECUTABLE=./driver
DEFAULTS=$(($GAMMA+$MTILDE))
ALL=$(($MULT+$MOD+$GAMMA+$MTILDE))
MULT_ONLY=$(($MULT+$DEFAULTS))
MOD_ONLY=$(($MOD+$DEFAULTS))

#NUMCOEFFS=3
POLY_DEG_N=1024
declare -a Q_VALUES=("16000" "32000")
ITERATIONS=50
PLAIN_MODULUS=32
#RESULTS_OUTFILE=./results/out.txt

for log_q in "${Q_VALUES[@]}"
do
  mkdir -p ./results/huge_q/${log_q}/
  echo "log q: ${log_q}"
  #With multithreading
  time $EXECUTABLE -i $ITERATIONS -n $POLY_DEG_N -q $log_q -t $PLAIN_MODULUS -o $DEFAULTS > ./results/huge_q/${log_q}/defaults.txt
  time $EXECUTABLE -i $ITERATIONS -n $POLY_DEG_N -q $log_q -t $PLAIN_MODULUS -o $ALL > ./results/huge_q/${log_q}/all.txt
  #Without multithreading
  time $EXECUTABLE -i $ITERATIONS -n $POLY_DEG_N -q $log_q -t $PLAIN_MODULUS -o $DEFAULTS -d > ./results/huge_q/${log_q}/defaults_nothreads.txt
  time $EXECUTABLE -i $ITERATIONS -n $POLY_DEG_N -q $log_q -t $PLAIN_MODULUS -o $ALL -d > ./results/huge_q/${log_q}/all_nothreads.txt
  python stats.py ./results/incr_q/${log_q}/*.txt > ./results/huge_q/${log_q}/${log_q}_report.rep
done

