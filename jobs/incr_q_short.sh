#!/bin/bash

#$ -M jtakeshi@nd.edu
#$ -m abe
#$ -q *@@jung
#$ -pe smp 1
#$ -N short_q_test
#

MULT=1
MOD=2
GAMMA=4
MTILDE=8
#Run from root directory
EXECUTABLE=./driver
DEFAULTS=$(($GAMMA+$MTILDE))
ALL=$(($MULT+$MOD+$GAMMA+$MTILDE))
MULT_ONLY=$(($MULT+$DEFAULTS))
MOD_ONLY=$(($MOD+$DEFAULTS))

#NUMCOEFFS=3
POLY_DEG_N=1024
declare -a Q_VALUES=("128" "256")
ITERATIONS=1000
PLAIN_MODULUS=32
#RESULTS_OUTFILE=./results/out.txt

for log_q in "${Q_VALUES[@]}"
do
  $EXECUTABLE -i $ITERATIONS -n $POLY_DEG_N -q "$log_q" -t $PLAIN_MODULUS -o $DEFAULTS > ./results/short/${log_q}/defaults.txt
  $EXECUTABLE -i $ITERATIONS -n $POLY_DEG_N -q "$log_q" -t $PLAIN_MODULUS -o $MULT_ONLY > ./results/short/${log_q}/mult.txt
  $EXECUTABLE -i $ITERATIONS -n $POLY_DEG_N -q "$log_q" -t $PLAIN_MODULUS -o $MOD_ONLY > ./results/short/${log_q}/mod.txt
  $EXECUTABLE -i $ITERATIONS -n $POLY_DEG_N -q "$log_q" -t $PLAIN_MODULUS -o $ALL > ./results/short/${log_q}/all.txt
  python stats.py ./results/incr_q/${log_q}/*.txt > ./results/short/${log_q}/${log_q}_report.rep
done

