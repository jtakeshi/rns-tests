#!/bin/bash

#$ -M jtakeshi@nd.edu
#$ -m abe
#$ -q *@@jung
#$ -pe smp 4
#$ -N HEStandard_8192
#

MULT=1
MOD=2
GAMMA=4
MTILDE=8
#Run from root directory
EXECUTABLE=./driver
DEFAULTS=$(($GAMMA+$MTILDE))
ALL=$(($MULT+$MOD+$GAMMA+$MTILDE))
MULT_ONLY=$(($MULT+$DEFAULTS))
MOD_ONLY=$(($MOD+$DEFAULTS))

#Values chosen to ensure 128 bits of security from HE standard

#NUMCOEFFS=3
#POLY_DEG_N=1024
#declare -a N_VALUES=("1024" "2048" "4096" "8192" "16384")
poly_deg=8192
#declare -a Q_VALUES=("8192", "16384", "32678")
Q_MOD=218
ITERATIONS=100
PLAIN_MODULUS=32
SUBFOLDER=he_standard_8192
#RESULTS_OUTFILE=./results/out.txt

mkdir -p ./results/${SUBFOLDER}/

#With multithreading
time $EXECUTABLE -i $ITERATIONS -n $poly_deg -q $Q_MOD -t $PLAIN_MODULUS -o $DEFAULTS > ./results/${SUBFOLDER}/defaults.txt
time $EXECUTABLE -i $ITERATIONS -n $poly_deg -q $Q_MOD -t $PLAIN_MODULUS -o $ALL > ./results/${SUBFOLDER}/all.txt
#Without multithreading
time $EXECUTABLE -i $ITERATIONS -n $poly_deg -q $Q_MOD -t $PLAIN_MODULUS -o $DEFAULTS -d > ./results/${SUBFOLDER}/defaults_nothreads.txt
time $EXECUTABLE -i $ITERATIONS -n $poly_deg -q $Q_MOD -t $PLAIN_MODULUS -o $ALL -d > ./results/${SUBFOLDER}/all_nothreads.txt

python stats.py ./results/${SUBFOLDER}/*.txt > ./results/${SUBFOLDER}/HES_report.rep


