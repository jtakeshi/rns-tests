#!/bin/bash

#$ -M jtakeshi@nd.edu
#$ -m abe
#$ -q *@@jung
#$ -pe smp 1
#$ -N RNS_optimizations
#

MULT=1
MOD=2
GAMMA=4
MTILDE=8
#Run from root directory
EXECUTABLE=./driver
DEFAULTS=$(($GAMMA+$MTILDE))
ALL=$(($MULT+$MOD+$GAMMA+$MTILDE))
MULT_ONLY=$(($MULT+$DEFAULTS))
MOD_ONLY=$(($MOD+$DEFAULTS))

NUMCOEFFS=3
POLY_DEG_N=1024
Q_NUMBITS=256
ITERATIONS=10000
PLAIN_MODULUS=32
RESULTS_OUTFILE=./results/out.txt

#No optimizations
$EXECUTABLE -i $ITERATIONS -n $POLY_DEG_N -q $Q_NUMBITS -t $PLAIN_MODULUS -k $NUMCOEFFS -o $DEFAULTS > ./results/defaults.txt
#Fast multiplication
$EXECUTABLE -i $ITERATIONS -n $POLY_DEG_N -q $Q_NUMBITS -t $PLAIN_MODULUS -k $NUMCOEFFS -o $MULT_ONLY > ./results/mult.txt
#Fast modulo
$EXECUTABLE -i $ITERATIONS -n $POLY_DEG_N -q $Q_NUMBITS -t $PLAIN_MODULUS -k $NUMCOEFFS -o $MOD_ONLY > ./results/mod.txt
#Both
$EXECUTABLE -i $ITERATIONS -n $POLY_DEG_N -q $Q_NUMBITS -t $PLAIN_MODULUS -k $NUMCOEFFS -o $ALL > ./results/all.txt

#Might as well have the CRC do the stats work as well
python stats.py ./results/*.txt > ./results/stats/report.txt
