#!/bin/bash

#$ -M jtakeshi@nd.edu
#$ -m abe
#$ -q *@@jung
#$ -pe smp 4
#$ -N inc_n
#

MULT=1
MOD=2
GAMMA=4
MTILDE=8
#Run from root directory
EXECUTABLE=./driver
DEFAULTS=$(($GAMMA+$MTILDE))
ALL=$(($MULT+$MOD+$GAMMA+$MTILDE))
MULT_ONLY=$(($MULT+$DEFAULTS))
MOD_ONLY=$(($MOD+$DEFAULTS))

#NUMCOEFFS=3
#POLY_DEG_N=1024
declare -a N_VALUES=("1024" "2048" "4096" "8192" "16384")
#declare -a N_VALUES=("1024" "2048" "4096")
#declare -a Q_VALUES=("8192", "16384", "32678")
Q_MOD=881
ITERATIONS=50
PLAIN_MODULUS=32
SUBFOLDER=increasing_n
#RESULTS_OUTFILE=./results/out.txt

for poly_deg in "${N_VALUES[@]}"
do
  mkdir -p ./results/${SUBFOLDER}/${poly_deg}/
  #With multithreading
  time $EXECUTABLE -i $ITERATIONS -n $poly_deg -q $Q_MOD -t $PLAIN_MODULUS -o $DEFAULTS > ./results/${SUBFOLDER}/${poly_deg}/defaults.txt
  time $EXECUTABLE -i $ITERATIONS -n $poly_deg -q $Q_MOD -t $PLAIN_MODULUS -o $ALL > ./results/${SUBFOLDER}/${poly_deg}/all.txt
  #Without multithreading
  time $EXECUTABLE -i $ITERATIONS -n $poly_deg -q $Q_MOD -t $PLAIN_MODULUS -o $DEFAULTS -d > ./results/${SUBFOLDER}/${poly_deg}/defaults_nothreads.txt
  time $EXECUTABLE -i $ITERATIONS -n $poly_deg -q $Q_MOD -t $PLAIN_MODULUS -o $ALL -d > ./results/${SUBFOLDER}/${poly_deg}/all_nothreads.txt
  python stats.py ./results/${SUBFOLDER}/${poly_deg}/*.txt > ./results/${SUBFOLDER}/${poly_deg}/${poly_deg}_report.rep
done

