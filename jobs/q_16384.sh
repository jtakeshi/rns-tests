#!/bin/bash

#$ -M jtakeshi@nd.edu
#$ -m abe
#$ -q *@@jung
#$ -pe smp 1
#$ -N q_16384
#

MULT=1
MOD=2
GAMMA=4
MTILDE=8
#Run from root directory
EXECUTABLE=./driver
DEFAULTS=$(($GAMMA+$MTILDE))
ALL=$(($MULT+$MOD+$GAMMA+$MTILDE))
MULT_ONLY=$(($MULT+$DEFAULTS))
MOD_ONLY=$(($MOD+$DEFAULTS))

#NUMCOEFFS=3
POLY_DEG_N=1024
declare -a Q_VALUES=("16384")
ITERATIONS=1000
PLAIN_MODULUS=32
SUBFOLDER=large_q
#RESULTS_OUTFILE=./results/out.txt

for log_q in "${Q_VALUES[@]}"
do
  $EXECUTABLE -i $ITERATIONS -n $POLY_DEG_N -q "$log_q" -t $PLAIN_MODULUS -o $DEFAULTS > ./results/${SUBFOLDER}/${log_q}/defaults.txt
  $EXECUTABLE -i $ITERATIONS -n $POLY_DEG_N -q "$log_q" -t $PLAIN_MODULUS -o $MULT_ONLY > ./results/${SUBFOLDER}/${log_q}/mult.txt
  $EXECUTABLE -i $ITERATIONS -n $POLY_DEG_N -q "$log_q" -t $PLAIN_MODULUS -o $MOD_ONLY > ./results/${SUBFOLDER}/${log_q}/mod.txt
  $EXECUTABLE -i $ITERATIONS -n $POLY_DEG_N -q "$log_q" -t $PLAIN_MODULUS -o $ALL > ./results/${SUBFOLDER}/${log_q}/all.txt
  python stats.py ./results/${SUBFOLDER}/${log_q}/*.txt > ./results/${SUBFOLDER}/${log_q}/${log_q}_report.rep
done

