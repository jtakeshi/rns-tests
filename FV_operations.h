
#include <vector>
#include <utility>

#include <NTL/ZZ.h>
#include <NTL/ZZX.h>


#ifdef DEBUG
# include <cassert>
#endif

using namespace std;
using namespace NTL;

using relin_key_t = std::pair<std::vector<ZZX>, std::vector<ZZX> >;

inline ZZ DWR(const ZZ & num, const ZZ & den){
#ifdef DEBUG
	assert((den != 0) && "Must have nonzero denominator!");
#endif	
	ZZ tmp = den >> 1;
	//TODO check formula for when num and den are both negative
	return ((num < 0) && (den < 0))? (num-(tmp))/den : (num+(tmp))>>1;
}

//Simple implementation for now
inline ZZ cmod_simple(const ZZ & arg, const ZZ & mod){
#ifdef DEBUG
	assert((mod > 0) && "Modulus should be > 0!");
#endif	
	ZZ rem(arg);
	ZZ bound;
	if(rem >= 0){
		bound = mod>>1;
		while(rem >= bound){
			rem -= mod;
		}
	}
	else{
		bound = -(mod>>1);
		while(rem < bound){
			rem += mod;
		}
	}
	return rem;
}

inline ZZ cmod_subtrahend(const ZZ & arg, const ZZ & mod){
	ZZ upper_bound = mod>>1;
	ZZ lower_bound = -upper_bound;
	//If we're already in range, we don't have to do anything
	if((arg < upper_bound) && (arg >= lower_bound)){
		return arg;
	}
	return mod*DWR(arg, mod);
}

#define HANDROLLED_CMOD 1

#if HANDROLLED_CMOD
inline ZZ cmod(const ZZ & arg, const ZZ & mod){
#ifdef DEBUG
	assert((mod > 0) && "Modulus should be > 0!");
#endif	
	return arg - cmod_subtrahend(arg, mod);
}

//TODO write in place cmod
inline void cmod_inplace(ZZ & arg, const ZZ & mod){
	#ifdef DEBUG
	assert((mod > 0) && "Modulus should be > 0!");
#endif	
	arg -= cmod_subtrahend(arg, mod);
}

#else
//These may not actually be correct - check for cases when arg is negative
inline ZZ cmod(const ZZ & arg, const ZZ & mod){
#ifdef DEBUG
	assert((mod > 0) && "Modulus should be > 0!");
#endif	
	ZZ tmp = arg % mod;
	if(tmp >= (mod >> 1)){
		tmp -= mod;
	}
	return tmp;
}

//TODO write in place cmod
inline void cmod_inplace(ZZ & arg, const ZZ & mod){
	#ifdef DEBUG
	assert((mod > 0) && "Modulus should be > 0!");
#endif	
	arg %= mod;
	if(arg >= (mod >> 1)){
		arg -= mod;
	}
	return;
}
#endif





inline ZZX poly_ring_mult(const ZZX & a, const ZZX & b, const ZZ & q){
	long n = max(deg(a), deg(b));
#ifdef DEBUG
	assert(q > 0 && n > 0);
	assert(deg(a) >= n);
	assert(deg(b) >= n);
	//n should also be a power of two
#endif	
	ZZX result = a*b;
	long degree = deg(result);
	for( ; degree >= 0; degree--){
		if(degree > n){
			if((degree/n)%2){
				result[degree % n] -= result[degree];
			}
			else{
				result[degree % n] += result[degree];
			}
			//result[degree] = 0;
		}
		else{
			result[degree] = cmod(result[degree], q);
		}
	}
	//result.normalize();
	trunc(result, result, degree);
	return result;
}

inline ZZX poly_add_mult(const ZZX & a, const ZZX & b, const ZZ & q){
	ZZX result = a+b;
	long degree = deg(result);
	for(long i = 0; i < degree; i++){
		result[i] = cmod(result[i], q);
	}
	return result;
}

inline void poly_add_mult_inplace(ZZX & a, const ZZX & b, const ZZ & q){
	long degree = max(deg(a), deg(b));
	a.SetLength(degree);
	for(long i = 0; i < degree; i++){
		a[i] += coeff(b, i);
		a[i] = cmod(a[i], q);
	}
	return;
}

ZZX FV_decrypt(const ZZX & sk, const vector<ZZX> & ct, const ZZ & t, const ZZ & q){
#ifdef DEBUG
	assert(ct.size() == 2);
#endif	
	//Compute polynomial operations
	//TODO polynomial modulus
	//ZZX result = ct[1] * sk;
	ZZX result = poly_ring_mult(ct[1], sk, q);
	result += ct[0];
	//TODO try the NTL range on this
	long poly_degree = deg(result);
	for(long i = 0; i < poly_degree; i++){
		//Less overflow here, so this may be faster?
		result[i] = cmod_simple(result[i], q);
		result[i] *= t;
		//Could write polynomial-arg functions for these, but this is more efficient
		result[i] = DWR(result[i], q);
		result[i] = cmod(result[i], t);
	}
	return result;
}

//No relinearization!!!
//Using std::vector for ciphertexts
vector<ZZX> FV_mul(const vector<ZZX> & ct1, 
	const vector<ZZX> ct2,  const ZZ & t, const ZZ & q){
#ifdef DEBUG
	assert(ct1.size() == 2);
	assert(ct2.size() == 2);
#endif
	vector<ZZX> c(ct1.size() + ct2.size() - 1);
	/*
	c[0] = ct1[0] * ct2[0];
	c[1] = (ct1[0]*ct2[1])+(ct1[1]*ct2[0]);
	c[2] = ct1[1] * ct2[1];
	*/
	c[0] = poly_ring_mult(ct1[0], ct2[0], q);
	c[1] = poly_add_mult(poly_ring_mult(ct1[0], ct2[1], q), poly_ring_mult(ct1[1], ct2[0], q), q);
	c[2] = poly_ring_mult(ct1[1], ct2[1], q);

	for(size_t i = 0; i < c.size(); i++){
		long poly_degree = deg(c[i]);
		for(long j = 0; j < poly_degree; j++){
			c[i][j] *= t;
			c[i][j] = DWR(c[i][j], q);
			c[i][j] = cmod(c[i][j], q);
		}
	}
	return c;
}

/*
Decomposition:
while(x){
	digits.push_back(x%T);
	x /= t;
}
*/

//The length of the relin key is equal to floor(log_t(q))

vector<ZZ> base_decomp(const ZZ & arg, const ZZ & base){
	vector<ZZ> ret;
	ZZ arg_copy = arg;
	while(arg_copy != 0){
		ret.push_back(arg_copy % base);
		arg_copy /= base;
	}
	return ret;
}


vector<ZZX> base_decomp(const ZZX & arg, const ZZ & base, unsigned int i){
	//Resulting vector is the base decompositions - assume index i is known
	vector<ZZX> ret(i, arg);
	ZZ arg_copy;
	for(unsigned int j = 0; j < deg(arg); j++){
		arg_copy = arg[j];
		for(unsigned int base_idx = 0; base_idx < i; base_idx++){
			ret[base_idx][j] = arg_copy % base;
			arg_copy /= base;
		}
	}
	return ret;
}


vector<ZZX> FV_relin(const vector<ZZX> & c, 
	const relin_key_t & rlk, const ZZ & base, const ZZ & q){
	vector<ZZX> c_prime(2);
	c_prime[0] = c[0];
	c_prime[1] = c[1];
#ifdef DEBUG
	assert(rlk.first.size() == rlk.second.size());
#endif	
	//MAC of base-decomposed c2 with rlk
	vector<ZZX> c2_decomposed = base_decomp(c[2], base, rlk.first.size());
	//Why <= ?
	for(size_t ell= 0; ell <= rlk.first.size(); ell++){
		c_prime[0] += rlk.first[ell]*c2_decomposed[ell];
		c_prime[1] += rlk.second[ell]*c2_decomposed[ell];
	}
	//Mod by q
	long deg_0 = deg(c_prime[0]);
	for(long deg_idx_0 = 0; deg_idx_0 < deg_0; deg_idx_0++){
		c_prime[0][deg_idx_0] = cmod(c_prime[0][deg_idx_0], q);
	}
	long deg_1 = deg(c_prime[1]);
	for(long deg_idx_1 = 0; deg_idx_1 < deg_1; deg_idx_1++){
		c_prime[1][deg_idx_1] = cmod(c_prime[1][deg_idx_1], q);
	}
	return c_prime;
}

vector<ZZX> FV_mult_and_relin(const vector<ZZX> & ct1, 
	const vector<ZZX> & ct2, const ZZ & t, const ZZ & q, 
	const relin_key_t & rlk, const ZZ & base){
	vector<ZZX> tensor_result = FV_mul(ct1, ct2, t, q);
	return FV_relin(tensor_result, rlk, base, q);
}