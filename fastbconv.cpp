#include <iostream>
#include <cassert>
#include <sstream>

#include <gmp.h>
#include <gmpxx.h>

#include "utilities.h"

#define DEBUG 0

//Debug settings
#if DEBUG
const unsigned int K_NUMBITS = 5;
const unsigned int NUMTRIALS = 100;
#else
//Timing settings
const unsigned int K_NUMBITS = 1000;
const unsigned int NUMTRIALS = 100000000;
#endif

//Number of inputs/bases
const unsigned int NUM_INPUTS = 2;
const unsigned int NUM_BASES = 1;

const unsigned int CLOCKS_PER_MS = (CLOCKS_PER_SEC/1000);

#define TIME_OPERATIONS 1
//Macro to log timing
//string passed must have no spaces in the interior, and at least one trailing
#if TIME_OPERATIONS
#define LOG(op,st,dur,stream,type) { \
	st = clock(); \
	op; \
	dur = (clock() - start)/(double) CLOCKS_PER_MS; \
	stream << type << duration << std::endl; \
}
#else
#define LOG(op,st,dur,stream,type) { op; }
#endif

using namespace std;

int main(int argc, char ** argv){

	ostringstream os;
	bool test_primes = false;
	bool test_powers_two = false;
	bool test_powers_two_optimized = false;
	//Get mode
	if(argc != 2){
		cout << "Enter exactly one of p (primes), t (power of two), o (optimized power-of-two)" << endl;
		return 0;
	}
	switch(argv[1][0]){
		case 'p':{
			test_primes = true;
			break;
		}
		case 't':{
			test_powers_two = true;
			break;
		}
		case 'o':{
			test_powers_two_optimized = true;
			break;
		}
	}	
	if(!(test_primes || test_powers_two || test_powers_two_optimized)){
		cout << "Must give at least one option!" << endl;
		return 0;
	}

	//Force GMP to have enough precision with larger numbers of bits - used for floating point ops
	if(mpf_get_default_prec() < K_NUMBITS){
		mpf_set_default_prec(K_NUMBITS);
	}

	//Testing regular reconstruct_primes with actual primes
	//WARNING: sizes here are hardcoded
	vector<mpz_class> coprimes(NUM_INPUTS);
	mpz_class one = 1;
	if(test_primes){
		mpz_class lower_bound = (one << K_NUMBITS) - 1;
		mpz_nextprime(coprimes[0].get_mpz_t(), lower_bound.get_mpz_t());
		mpz_nextprime(coprimes[1].get_mpz_t(), coprimes[0].get_mpz_t());
	}
	else {
		coprimes[0] = (one << K_NUMBITS) + 1;
		coprimes[1] = (one << K_NUMBITS) - 1;
	}


	//Precompute product and inverses
	mpz_class coprimes_product = product(coprimes);
	vector<mpz_class> regular_mcands = CRT_mcands(coprimes);
	vector<mpz_class> inv_mod_mcands = inverses_reduced_regular(regular_mcands, coprimes);
	//Not yet reduced - just the product
	vector<mpz_class> partial_products = coeff_multiply(regular_mcands, inv_mod_mcands);

	//Create a coprime base - must be coprime to q
	vector<mpz_class> coprime_bases(NUM_BASES);
	coprime_bases[0] = (one << K_NUMBITS);

	
	//Get x and y to test - assume for now that these are modular products
	gmp_randstate_t state;
	gmp_randinit_default(state);
	mpz_class x;
	for(unsigned int i = 0; i < NUMTRIALS; i++){
		//Construct random input
		mpz_urandomb(x.get_mpz_t(), state, K_NUMBITS);
		//TODO force x to not be a multiple of q
		vector<mpz_class> inputs(NUM_INPUTS);
		for(unsigned int y = 0; y < inputs.size(); y++){
			inputs[y] = x % coprimes[y];
		}

		//DEBUG: print inputs
		/*
		cout << "Input: " << x << endl;
		cout << "RNS input: ";
		for(const auto & ip : inputs){
			cout << ip << ' ';
		}
		cout << endl << endl;
		*/

		double total_start = clock();
		vector<mpz_class> results(NUM_BASES);
		mpz_class total = 0;
		mpz_class subtotal;
		for(size_t j = 0; j < NUM_INPUTS; j++){
			//Multiply or bitshift
			if(test_powers_two_optimized){
				subtotal = inputs[j] << (K_NUMBITS-1);
			}
			else{
				subtotal = inputs[j]*regular_mcands[j];
			}
			//Reduce
			subtotal %= coprimes[j];
			//Multiply
			if(test_powers_two_optimized){
				//This should be faster (based on operation time alone)
				mpz_class old_subtotal = subtotal;
				subtotal <<= K_NUMBITS;
				//WARNING: only works when NUM_INPUTS is 2
				if(!j){
					//q/q_i = (2^2k - 1)/(2^k + 1) = 2^k - 1
					subtotal -= old_subtotal;
				}
				else{
					//q/q_i = (2^2k - 1)/(2^k - 1) = 2^k + 1
					subtotal += old_subtotal;
				}
			}
			else{
				subtotal *= regular_mcands[j];
			}
			//Add to running total 
			total += subtotal;
		}			
		for(size_t k = 0; k < NUM_BASES; k++){
			//Error checking
			/*
#if DEBUG
			if(total == 0){
				//Check if x is a multiple of q
				mpz_class gcd;
				mpz_gcd(gcd.get_mpz_t(), total.get_mpz_t(), coprimes_product.get_mpz_t());
				assert(gcd == 1 || x == 0);
			}
			//assert(total != 0);
			assert(coprime_bases[k] != 0);
#endif			
			*/
			results[k] = total % coprime_bases[k];
		}
		double total_duration = (clock() - total_start)/(double) CLOCKS_PER_MS;
		os << total_duration << endl;

	}

	cout << os.str();
	
	return 0;
}