//Compile as: g++ -pthread -I ~/.local/lib/include/ -std=c++17 fast_mul_mod.cpp -o fast_mul_mod -L ~/.local/lib/lib -lseal -lgmp -lgmpxx -O3
#include <iostream>
#include <cassert>
#include <sstream>
#include <chrono>
#include <bitset>

#include <gmp.h>
#include <gmpxx.h>

#include <seal/seal.h>

#include "utilities.h"

#define DEBUG 0

//Debug settings
#if DEBUG
const unsigned int K_NUMBITS = 5;
const unsigned int NUMTRIALS = 100;
#else
//Timing settings
const unsigned int K_NUMBITS = 32;
const unsigned int NUMTRIALS = 100000000;
#endif

//Number of inputs/bases
const unsigned int NUM_BASES = 2;
const unsigned int BASE_TO_RUN = 0;

const bool CHECK_CORRECTNESS = true;

using namespace std;
using namespace seal;
using namespace seal::util;

using number_t = uint64_t;

int main(int argc, char ** argv){
	//Basic checks
	assert(BASE_TO_RUN < NUM_BASES);

	//Set randomness
	srand(1);

	ostringstream os;
	bool test_primes = false;
	bool test_powers_two = false;
	bool test_powers_two_optimized = false;
	//Get mode
	if(argc != 2){
		cout << "Enter exactly one of p (primes), t (power of two), o (optimized power-of-two)" << endl;
		return 0;
	}
	switch(argv[1][0]){
		case 'p':{
			test_primes = true;
			break;
		}
		case 't':{
			test_powers_two = true;
			break;
		}
		case 'o':{
			test_powers_two_optimized = true;
			break;
		}
	}	
	if(!(test_primes || test_powers_two || test_powers_two_optimized)){
		cout << "Must give at least one option!" << endl;
		return 0;
	}

	//Timing
	std::chrono::steady_clock::time_point start, end;

	string optype = test_powers_two? "optimized" : "regular";
	if(test_powers_two_optimized){
		optype += "_explicit";
	}

	//Testing regular reconstruct_primes with actual primes
	//WARNING: sizes here are hardcoded
	vector<number_t> coprimes(NUM_BASES);
	if(test_primes){
		mpz_class lower_bound = 1;
		lower_bound = lower_bound << K_NUMBITS;
		lower_bound--;
		mpz_class coprime_tmp;
		mpz_nextprime(coprime_tmp.get_mpz_t(), lower_bound.get_mpz_t());
		coprimes[0] = mpz_get_ui(coprime_tmp.get_mpz_t());
		mpz_nextprime(coprime_tmp.get_mpz_t(), coprime_tmp.get_mpz_t());
		coprimes[1] = mpz_get_ui(coprime_tmp.get_mpz_t());
	}
	else {
		//First coprime is the larger one
		//Set to 2y+-1 = 2^(k)+-1
		coprimes[0] = coprimes[1] = 1;
		coprimes[0] <<= (K_NUMBITS);
		coprimes[1] <<= (K_NUMBITS);
		coprimes[0]++;
		coprimes[1]--;
	}

	number_t y = 1;
	y <<= (K_NUMBITS-1); //y=2^(k-1)
	
	//Get x and y to test - assume for now that these are modular products
	gmp_randstate_t state;
	gmp_randinit_default(state);;
	SmallModulus sm(coprimes[BASE_TO_RUN]);
	mpz_class gmp_prod, gmp_shifted, gmp_shifted_prod, gmp_sub, gmp_final;
	for(unsigned int i = 0; i < NUMTRIALS; i++){
		//Preprocessing

		number_t mult_result[2];
		number_t sub_result[2];
		number_t final_result[2];
		number_t shift_result[2];
		number_t premult_shifted[2];
		final_result[0] = final_result[1] = 0;
		mult_result[0] = mult_result[1] = 0;
		sub_result[0] = sub_result[1] = 0;
		shift_result[0] = shift_result[1] = 0;
		premult_shifted[0] = premult_shifted[1] = 0;

		//Construct random input
		number_t x = rand();
		x %= coprimes[BASE_TO_RUN];
		
		if(test_powers_two_optimized){
			start = std::chrono::steady_clock::now();
			//1. Calculate product x*y = x*2^k
			//1a. Shift x by k bits
			mult_result[0] = mult_result[1] = x;
			mult_result[0] <<= (K_NUMBITS-1);
			mult_result[1] >>= ((8*sizeof(number_t)) - (K_NUMBITS-1));
			
#if DEBUG
			if(CHECK_CORRECTNESS){
				gmp_prod = uint64_to_mpz(mult_result, 2);
				cout << "x*y: " << gmp_prod << endl;
			}
#endif			

			//2. Find subtrahend (x>>1)*(2y+-1)
			//2a. First shift x or x-1 right by 1
			number_t x_shifted = x;
			//Subtract 1 if the modulus is 2y+1
			if((BASE_TO_RUN == 0) && x_shifted){
				x_shifted--;
			}
			x_shifted >>= 1;
#if DEBUG			
			if(CHECK_CORRECTNESS){
				gmp_shifted = x_shifted;
				cout << (BASE_TO_RUN==0? "((x-1)>>1): " 
							: "(x>>1): ") << gmp_shifted << endl;
			}
#endif			
			//2b. Shift the result left by k bits, as 2y = 2^k
			shift_result[0] = shift_result[1] = x_shifted;
			shift_result[0] <<= K_NUMBITS;
			shift_result[1] >>= ((8*sizeof(number_t))-K_NUMBITS);
#if DEBUG			
			if(CHECK_CORRECTNESS){
				gmp_shifted_prod = uint64_to_mpz(shift_result);
				cout << (BASE_TO_RUN==0? "(2y)*((x-1)>>1): " 
							: "(2y)*(x>>1): ") << gmp_shifted_prod << endl;
			}
#endif			
			//2c. Then add/subtract (x_shifted>>1)
			//May need to have yet another result for output - check correctness to be sure
			//Shouldn't need to check output - remove carry later
#if DEBUG			
			unsigned char carry;
#endif			
			if(BASE_TO_RUN == 0){
#if DEBUG
				carry = add_uint_uint64(shift_result, x_shifted, 2, sub_result);
#else
				add_uint_uint64(shift_result, x_shifted, 2, sub_result);
#endif				
			}
			else{
#if DEBUG				
				carry = sub_uint_uint64(shift_result, x_shifted, 2, sub_result);
#else				
				sub_uint_uint64(shift_result, x_shifted, 2, sub_result);
#endif				
			}
#if DEBUG			
			if(CHECK_CORRECTNESS){
				gmp_sub = uint64_to_mpz(sub_result);
				cout << (BASE_TO_RUN==0? "(2y+1)*((x-1)>>1): " 
							: "(2y-1)*(x>>1): ") << gmp_sub << endl;
			}			
			if(CHECK_CORRECTNESS){
				assert(!carry);
			}
#endif			
			
			//3. Compute difference x*y-(2y+-1)*(x>>1)
#if DEBUG			
			unsigned char borrow;
			borrow = sub_uint_uint(mult_result, sub_result, 2, final_result);			
			if(CHECK_CORRECTNESS){
				assert(!borrow);
			}
			if(CHECK_CORRECTNESS){
				gmp_final = uint64_to_mpz(final_result);
				cout << "Final: " << gmp_final << endl;
			}
#else
			sub_uint_uint(mult_result, sub_result, 2, final_result);	
#endif			
			end = std::chrono::steady_clock::now();
#if DEBUG
			if(CHECK_CORRECTNESS){
				assert(mult_result[1] >= sub_result[1]);
				if(mult_result[1] == sub_result[1]){
					if(!(mult_result[0] >= sub_result[0])){
						mpz_class mult_mpz = uint64_to_mpz(mult_result, 2);
						mpz_class sub_mpz = uint64_to_mpz(sub_result, 2);
						sub_mpz = sub_result[1];
						cout << "x*y: " << mult_mpz << endl;
						cout << (BASE_TO_RUN==0? "(2y+1)*((x-1)>>1): " 
							: "(2y-1)*(x>>1): ") << sub_mpz << endl;
						assert(mult_mpz >= sub_mpz);
					}
					assert(mult_result[0] >= sub_result[0]);
				}
				assert(!borrow);
			}

			if(CHECK_CORRECTNESS){
				if(final_result[1] != 0){
					cout << "x: " << x << endl;
					cout << "y: " << y << endl;
					cout << "modulus: " << coprimes[BASE_TO_RUN] << ' ' << sm.value() << endl;
					cout << "final_result: \t" << std::bitset<64>(final_result[0]) << ' ' << final_result[0]<< endl;
					cout << "actual: \t" << multiply_uint_uint_mod(x, y, sm) << endl;
				}
				assert(final_result[1] == 0);
				if(final_result[0] != multiply_uint_uint_mod(x, y, sm)){
					cout << "x: " << x << endl;
					cout << "y: " << y << endl;
					cout << "modulus: " << coprimes[BASE_TO_RUN] << ' ' << sm.value() << endl;
					cout << "final_result: \t" << std::bitset<64>(final_result[0]) << ' ' << final_result[0] << endl;
					cout << "actual: \t" << std::bitset<64>(multiply_uint_uint_mod(x, y, sm)) << ' ' << multiply_uint_uint_mod(x, y, sm) << endl;
				}
				assert(final_result[0] == multiply_uint_uint_mod(x, y, sm));
			}
#endif			
		}
		else{
			start = std::chrono::steady_clock::now();
			final_result[0] = multiply_uint_uint_mod(x, y, sm);
			end = std::chrono::steady_clock::now();
		}

		
		double total_duration = std::chrono::duration_cast<std::chrono::nanoseconds>(end-start).count();
		os << optype << ' ' << total_duration << endl;

	}

	cout << os.str();
	
	return 0;
}