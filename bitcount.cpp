#include <iostream>

#include <gmp.h>
#include <gmpxx.h>

#include "utilities.h"

using namespace std;

int main(int argc, char ** argv){
	unsigned int bits = mpf_get_default_prec();
	cout << "Default float precision bits: " << bits << endl;
	return 0;
}