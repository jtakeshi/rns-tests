#include <random>
#include <iostream>
#include <cassert>
#include <bitset>
#include <sstream>

#define DEBUG 0

#if DEBUG
const unsigned int NUMTRIALS = 10;
#else
const unsigned int NUMTRIALS = 100000000;
#endif

//const unsigned int K_NUMBITS = 64;
const unsigned int BITWIDTH = 64;


const unsigned int CLOCKS_PER_MS = (CLOCKS_PER_SEC/1000);

using namespace std;

//These functions copied from SEAL
template<typename T, typename S, typename R>
inline unsigned char add_uint64(
	    T operand1, S operand2, R *result){
	    *result = operand1 + operand2;
	    return static_cast<unsigned char>(*result < operand1);
	}

template<typename T, typename S>
inline void multiply_uint64_generic(
            T operand1, S operand2, unsigned long long *result128){
#if DEBUG
    if (!result128)
    {
        throw std::invalid_argument("result128 cannot be null");
    }
#endif
    auto operand1_coeff_right = operand1 & 0x00000000FFFFFFFF;
    auto operand2_coeff_right = operand2 & 0x00000000FFFFFFFF;
    operand1 >>= 32;
    operand2 >>= 32;

    auto middle1 = operand1 * operand2_coeff_right;
    T middle;
    auto left = operand1 * operand2 + (static_cast<T>(add_uint64(
        middle1, operand2 * operand1_coeff_right, &middle)) << 32);
    auto right = operand1_coeff_right * operand2_coeff_right;
    auto temp_sum = (right >> 32) + (middle & 0x00000000FFFFFFFF);

    result128[1] = static_cast<unsigned long long>(
        left + (middle >> 32) + (temp_sum >> 32));
    result128[0] = static_cast<unsigned long long>(
        (temp_sum << 32) | (right & 0x00000000FFFFFFFF));
}

//Candidate function to shift
//Assume we never shift by zero
inline void shift_uint64(uint64_t operand, uint64_t shift_amt, unsigned long long * result){
#if DEBUG
    if (!result)
    {
        throw std::invalid_argument("result cannot be null");
    }
    if (shift_amt > 64)
    {
        throw std::invalid_argument("shift_amt cannot be > 64");
    }
#endif
    result[0] = operand << shift_amt;
    result[1] = operand >> (64 - shift_amt);
    /*
    if(shift_amt){
    	result[1] = operand >> (64 - shift_amt);
    }
    else{
    	result[1] = 0;
    }
    */
}


int main(int argc, char ** argv){

	bool regular_alg = true;
	bool correctness = false;

	//Get mode
	if(argc != 2){
		cout << "Enter exactly one of r (regular alg.), o (optimized alg.), c (check correctness)" << endl;
		return 0;
	}
	switch(argv[1][0]){
		case 'r':{
			regular_alg = true;
			break;
		}
		case 'o':{
			regular_alg = false;
			break;
		}
		case 'c':{
			correctness = true;
			break;
		}
		default:{
			cout << "Unrecognized option: " << argv[1] << endl;
			return 0;
		}
	}	

	ostringstream os;

	std::random_device rd;
	std::mt19937_64 mersenne(rd());
	/*
	srand(1);
	std::mt19937_64 mersenne(rand());
	*/
	std::uniform_int_distribution<uint64_t> dist(0, (uint64_t) -1);

	for(unsigned int i = 0; i < NUMTRIALS; i++){
		uint64_t shift_amt, mcand, mplier;
		unsigned long long result[2];
		double start, duration;
		mcand = dist(mersenne);
		shift_amt = 0;
		//Ensure shift amount is never zero
		while(!shift_amt){
			shift_amt = dist(mersenne) % BITWIDTH;
		}
		assert(shift_amt && shift_amt < 64);
		//WARNING: trying to directly assign a uint64_t as (1<<shift_amt) results in an error
		//Likely due to the compiler trying to interpret stuff as literals
		mplier = 1;
		mplier <<= shift_amt;
		//Test correctness
		if(correctness){
			//Do computation
			unsigned long long result_regular[2];
			shift_uint64(mcand, shift_amt, result);
			multiply_uint64_generic(mcand, mplier, result_regular);
			//Check correctness
			if(result[0] != result_regular[0] || result[1] != result_regular[1]){
				cout << "mcand\t\t" << std::bitset<64>(mcand) << endl;
				cout << "shift_amt " << shift_amt << endl;
				cout << "mplier\t\t" << std::bitset<64>(mplier) << endl;
				cout << "result\t\t" << std::bitset<64>(result[1]) << ' ' << std::bitset<64>(result[0]) << endl;
				cout << "result_regular\t" << std::bitset<64>(result_regular[1]) << ' ' << std::bitset<64>(result_regular[0]) << endl;
			}

			assert(result[0] == result_regular[0]);
			assert(result[1] == result_regular[1]);
		}
		else{
			if(regular_alg){
				start = clock();
				multiply_uint64_generic(mcand, mplier, result);
				duration = clock();
			}
			else{
				start = clock();
				shift_uint64(mcand, shift_amt, result);
				duration = clock();
			}
			double total_duration = (duration - start)/(double) CLOCKS_PER_MS;
			os << total_duration << '\n';
		}
	}

	cout << os.str();
}






