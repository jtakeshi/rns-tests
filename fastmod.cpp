#include <iostream>
#include <signal.h>
#include <gmp.h>
#include <gmpxx.h>


#include "utilities.h"

using namespace std;

#define QUICKDWR 1

const unsigned int K_UPPERBOUND = 16;

static mpz_class last_k_failed_at = -1;
static mpz_class last_k_achieved = -1;

const unsigned int CLOCKS_PER_MS = (CLOCKS_PER_SEC/1000);

void handle_int(int signo){
	cout << "Last k failed at: " << last_k_failed_at << endl;
	cout << "Last k achieved: " << last_k_achieved << endl;
	exit(signo);
}



int main(int argc, char ** argv){
	//Register signal handler
	//signal(SIGINT, handle_int);

	mpz_class y, x, k_minus_one, shifted, mod, rounded, modded, 
	upper_bound, lower_bound,
	num, den, bound, prod, result;
	double start, end;
	
	//Already know it fails for k \approx 32
	for(k_minus_one = 1; k_minus_one < K_UPPERBOUND; k_minus_one++){
		y = 1;
		mpz_mul_2exp(y.get_mpz_t(), y.get_mpz_t(), mpz_get_ui(k_minus_one.get_mpz_t()));
		//y = y << (k-1);
		mod = (2*y)+1;
		lower_bound = -mod/2;
		upper_bound = mod/2;
		for(x = lower_bound; x < upper_bound; x++){
			start = clock();
#if QUICKDWR
			result = x/2;
#else
			num = x*y;
			den = (2*y)+1;
			result = DWR(num, den);
#endif			
			end = clock();
			cout << (duration - start)/(double) CLOCKS_PER_MS << endl;			
		}
	}
	/*
	cout << "Last k failed at: " << last_k_failed_at << endl;
	cout << "Last k achieved: " << last_k_achieved << endl;
	*/
	cout << "Finished" << endl;
}