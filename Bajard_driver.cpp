// g++ Bajard_driver.cpp -pthread -lntl -lgmp -O3 -DDEBUG -Wall -Werror -pedantic -o driver

#include <iostream>
#include <unistd.h>
#include <cassert>
#include <signal.h>
#include <thread>

#include <NTL/ZZ.h>
#include <NTL/ZZX.h>
#include <NTL/BasicThreadPool.h>

#include "Bajard_operations.h"

/*
using namespace std;
using namespace NTL;
*/

//Sample args:
// -i 10 -n 512 -q 128 -t 32 -k 2 -o 9 -v

static const unsigned int MAX_THREADS = 4;

//Global so a signal handler can access it
static Bajard_operator bj;

void on_interrupt(int sig){
	cout << "Interrupted\n";
	bj.output_stream(std::cout);
}

int main(int argc, char ** argv){

	signal(SIGINT, on_interrupt);

	unsigned long num_iterations = 0;
	long poly_degree_N = 0;
	unsigned int q_numbits = 0;
	ZZ plain_modulus;
	//unsigned int num_coeffs = 0;
	unsigned int optimized = 0;
	bool verbose = false;
	//unsigned int m_power = 0;
	bool disable_threads = false;

	int c;
	while((c = getopt(argc, argv, "i:n:q:t:o:vk:m:d")) != -1){
		switch(c){
			case 'v':{
				verbose = true;
				break;
			}
			case 'i':{
				num_iterations = atoi(optarg);
				break;
			}
			case 'n':{
				poly_degree_N = atol(optarg);
				assert(poly_degree_N > 0);
				break;
			}
			case 'q':{
				q_numbits = atoi(optarg);
				break;
			}
			case 't':{
				plain_modulus = atoi(optarg);
				break;
			}
			/*
			case 'k':{
				num_coeffs = atoi(optarg);
				break;
			}
			*/
			case 'o':{
				if(optarg == nullptr){
					cout << "No option given!" << endl;
					return 0;
				}
				optimized = atoi(optarg);
				break;
			}
			/*
			case 'm':{
				m_power = atoi(optarg);
				break;
			}
			*/
			case 'd':{
				disable_threads = !disable_threads;
				break;
			}
			default:{
				cout << "Unrecognized option: " << c << endl;
				return 0;
			}
		}
	}

	unsigned int num_threads = std::thread::hardware_concurrency();
	if(!disable_threads && (num_threads > 1)){
		num_threads = max(num_threads, MAX_THREADS);
		SetNumThreads(num_threads);
	}
	if(verbose){
		cout << '#' << num_threads << " threads indicated" << endl;
		if(disable_threads){
			cout << "#Multithreading disabled" << endl;
		}
	}


	/*
	if((optimized & Bajard_operator::OPTIMIZE_MULT) 
		&& (optimized & Bajard_operator::OPTIMIZE_MOD) 
		&& Bajard_operator::opt_params(q_numbits, m_power, num_coeffs)){
		cout << "Error: parameters not determined correctly: ";
		cout << "|q| " << q_numbits << " m " << m_power << " k " << num_coeffs << endl;
		return 0;
	}
	*/

	//bj.init(q_numbits, poly_degree_N-1, plain_modulus, num_coeffs, optimized, m_power);
	bj.init(q_numbits, poly_degree_N-1, plain_modulus, optimized);
	if(verbose){
		cout << "#Finished initialization" << endl;
	}
	ctext x;
	ZZX dec_result;
	ctext mult_result;
	if(verbose){
		bj.print_moduli(std::cout);
		cout << "#Poly. degree: " << bj.poly_deg() << endl;
	}
	
	for(unsigned int i = 0; i < num_iterations; i++){
		x = bj.random_ciphertext();
		dec_result = bj.Bajard_decrypt(x, optimized);
		dec_result.kill();
		bj.write_timing(DECRYPT, optimized, verbose);
		/*
		if(verbose){
			cout << "Finished decryption " << i << endl;
		}
		*/
		mult_result = bj.Bajard_multiply(x, x, optimized);
		for(auto & x : mult_result.first){
			x.kill();
		}
		for(auto & x : mult_result.second){
			x.kill();
		}

		bj.write_timing(MULT, optimized, verbose);
		/*
		if(verbose){
			cout << "Finished mult. " << i << endl;
		}
		*/
	}

	bj.output_stream(std::cout);
	cout << endl;

	return 0;

}


/*
Multithreading:
poly_ring_mult /
star_optimized /
FBC decryption /
ctext in sec. key /
SmRQ /
relin NOT IMPLEMENTED AT THIS TIME
FBC floor /
fastRNSfloor

TODO handle tmp vars

*/