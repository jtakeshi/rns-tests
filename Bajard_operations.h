#include <math.h>

#include <vector>
#include <utility>
#include <iostream>
#include <sstream>
#include <ctime>
#include <chrono>

#include <NTL/ZZ.h>
#include <NTL/ZZX.h>
#include <NTL/BasicThreadPool.h>


#ifdef DEBUG
# include <cassert>
#endif

#include "FV_operations.h"
#include "optmod.h"

using namespace std;
using namespace std::chrono;
using namespace NTL;

using ctext = std::pair<std::vector<ZZX>, std::vector<ZZX> >;
using duration_t = std::chrono::high_resolution_clock::time_point;
//Utility functions


template<typename T>
ZZX poly_scale(const ZZX & arg, const T & scalar, long degree = 0){
	if(!degree){
		degree = deg(arg);
	}
	ZZX ret = arg;
	for(long i = 0; i <= degree; i++){
		ret[i] *= scalar;
	}
	return ret;
}

/*
void error_dist(ZZX & x, long deg){
	x.SetLength(deg+1);
	for(long i = 0; i <= deg; i++){
		int tmp = (rand() % 3) - 1;
		SetCoeff(x, i, tmp);
	}
	return;
}
*/

void error_dist(ZZX & x, long deg, const ZZ & modulus){
	x.SetLength(deg+1);
	for(long i = 0; i <= deg; i++){
		int tmp = (rand() % 3) - 1;
		if(tmp >= 0){
			SetCoeff(x, i, tmp);
			//x[i] = tmp;
		}
		else{
			SetCoeff(x, i, modulus-1);
			//x[i] = modulus-1;
		}
		//x[i] = (tmp >= 0)? ZZ(tmp) : modulus-1;
	}
	return;
}

void error_dist(vector<ZZX> & x, long deg, const vector<ZZ> & moduli){
#ifdef DEBUG
	assert(x.size() == moduli.size());
#endif	
	for(size_t i = 0; i < moduli.size(); i++){
		error_dist(x[i], deg, moduli[i]);
	}
	return;
}


void unif_vec(ZZX & x, long deg, const ZZ & modulus){
	assert(modulus > 0);
	x.SetLength(deg+1);
	//ZZ mod_bound = modulus/2;
	for(long i = 0; i <= deg; i++){
		SetCoeff(x, i, RandomBnd(modulus));
		//x[i] = RandomBnd(modulus);
	}
	return;
}

void unif_vec(vector<ZZX> & x, long deg, const ZZ & modulus){
	for(auto & y : x){
		unif_vec(y, deg, modulus);
	}
}

/*
inline ZZX inner_product(const ZZX & a, const ZZX & b){
	bool a_larger = degree(a) >= degree(b);
	ZZX ret = (a_larger)? a : b;
	const ZZX & other = (a_larger)? b : a;
	long deg = degree(ret);
	for(long i = 0; i < deg; i++){
		ret[i] *= coeff(other, i);
	}
	return ret;
}
*/

//Assume arguments are same length
inline ZZX inner_product(const vector<ZZX> & a, const vector<ZZX> & b){
	//vector<ZZX> ret(a.size());
	ZZX ret(0);
	ZZX tmp;
	for(size_t i = 0; i < a.size(); i++){
		mul(tmp, a[i], b[i]);
		ret += tmp;
	}
	return ret;
}

inline ZZX inner_product(const ZZX & a, const ZZX & b){
	ZZX ret;
	long degree = min(deg(a), deg(b));
	ret.SetLength(degree+1);
	NTL_EXEC_RANGE(degree+1, first, last)
	for(long i = first; i < last; i++){
		mul(ret[i], a[i], b[i]);
	}
	NTL_EXEC_RANGE_END
	return ret;
}

inline ZZX mod_poly(const ZZX & arg, const ZZ & modulus, const bool check_neg=true){
	ZZX ret = arg;
	long degree = deg(arg);
	if(degree == -1){
		return ret;
	}
	ret.SetLength(degree+1);
	for(long d = 0; d <= degree; d++){
		ret[d] %= modulus;
		if(check_neg){
			if(ret[d] < 0){
				ret[d] = modulus+arg[d];
			}
		}
	}
	return ret;
}

inline void mod_inplace(ZZX & arg, const ZZ & modulus, const bool check_neg = true){
	long degree = deg(arg);
	for(long d = 0; d <= degree; d++){
		arg[d] %= modulus;
		if(check_neg){
			if(arg[d] < 0){
				arg[d] = modulus+arg[d];
			}
		}
	}
	return;
}

inline void mod_inplace(vector<ZZX> & arg, const vector<ZZ> & moduli, const bool check_neg = true){
#ifdef DEBUG
	assert(arg.size() == moduli.size());
#endif	
	for(size_t i = 0; i < arg.size(); i++){
		mod_inplace(arg[i], moduli[i], check_neg);
	}
	return;
}

//Degree-2 arguments (hardcoded)
inline vector< vector<ZZX> > star_ordinary(const ctext & ct1, const ctext & ct2, const vector<ZZ> & moduli,
 const long poly_mod_deg, const vector<ZZ> & twiddle_dummies){
	vector< vector<ZZX> > ret(3);
	for(auto & v : ret){
		v.resize(moduli.size());
	}

	long num_components = moduli.size();
	NTL_EXEC_RANGE(num_components, first, last)
	for(long i = first; i < last; i++){
		//long g = m << (i ? i-1 : 0);
		
		ret[0][i] = poly_mult_NTT_unoptimized(ct1.first[i], ct2.first[i], poly_mod_deg, twiddle_dummies, twiddle_dummies, moduli[i]);
		ret[2][i] = poly_mult_NTT_unoptimized(ct1.second[i], ct2.second[i], poly_mod_deg, twiddle_dummies, twiddle_dummies, moduli[i]);

		ret[1][i] = poly_mult_NTT_unoptimized(ct1.first[i], ct2.second[i], poly_mod_deg, twiddle_dummies, twiddle_dummies, moduli[i]);
		poly_add_inplace(ret[1][i], poly_mult_NTT_unoptimized(ct1.second[i], ct2.first[i], poly_mod_deg, twiddle_dummies, twiddle_dummies, moduli[i]), moduli[i]);

	}
	NTL_EXEC_RANGE_END;
	return ret;
}

//In progress
inline vector< vector<ZZX> > star_optimized(const ctext & ct1, const ctext & ct2, 
	const vector<ZZ> & moduli, const unsigned int m, const long poly_mod_deg, const vector<ZZ> & twiddle_dummies){
#ifdef DEBUG
	assert(ct1.first.size() == ct2.first.size());
	assert(ct1.second.size() == ct2.second.size());
	assert(moduli.size() == ct2.second.size());
#endif	
	vector< vector<ZZX> > ret(3);
	for(auto & v : ret){
		v.resize(moduli.size());
	}
	long num_components = moduli.size();
	NTL_EXEC_RANGE(num_components, first, last)
	for(long i = first; i < last; i++){
		//long g = m << (i ? i-1 : 0);
		
		ret[0][i] = poly_mult_NTT_optimized(ct1.first[i], ct2.first[i], poly_mod_deg, twiddle_dummies, twiddle_dummies, moduli[i], m << (i ? i-1 : 0),	i);
		ret[2][i] = poly_mult_NTT_optimized(ct1.second[i], ct2.second[i], poly_mod_deg, twiddle_dummies, twiddle_dummies, moduli[i], m << (i ? i-1 : 0),	i);

		ret[1][i] = poly_mult_NTT_optimized(ct1.first[i], ct2.second[i], poly_mod_deg, twiddle_dummies, twiddle_dummies, moduli[i], m << (i ? i-1 : 0),	i);
		poly_add_inplace(ret[1][i], poly_mult_NTT_optimized(ct1.second[i], ct2.first[i], poly_mod_deg, twiddle_dummies, twiddle_dummies, moduli[i], m << (i ? i-1 : 0),	i), moduli[i]);
		//ret[1][i] += poly_mult_NTT_optimized(ct1.second[i], ct2.first[i], poly_mod_deg, twiddle_dummies, twiddle_dummies, moduli[i], m << (i ? i-1 : 0),	i);
		
	}
	NTL_EXEC_RANGE_END;
	return ret;
}

inline void cmod_inplace(ZZX & arg, const ZZ & modulus, long degree = 0){
	if(!degree){
		degree = deg(arg);
	}
	for(long d = 0; d <= degree; d++){
		cmod_inplace(arg[d], modulus);
	}
	return;
}

inline void cmod_inplace(vector<ZZX> & arg, const vector<ZZ> & moduli){
#ifdef DEBUG
	assert(arg.size() == moduli.size());
#endif	
	for(size_t i = 0; i < arg.size(); i++){
		cmod_inplace(arg[i], moduli[i]);
	}
	return;
}

inline ZZX cmod(const ZZX & arg, const ZZ & modulus, long degree = 0){
	ZZX ret(arg);
	//long degree = deg(ret);
	if(!degree){
		degree = deg(arg);
	}
	for(long d = 0; d <= degree; d++){
		cmod_inplace(ret[d], modulus);
	}
	return ret;
}

inline bool in_range(const ZZ & x, const ZZ & modulus){
	assert(modulus > 0);
	return (x >= 0) && (x < modulus);
}

inline bool in_range(const ZZX & x, const ZZ & modulus, long * idx = NULL){
	assert(modulus > 0);
	long degree = deg(x);
	for(long i = 0; i <= degree; i++){
		if((coeff(x, i) < 0) || (coeff(x, i) >= modulus)){
			if(idx != NULL){
				*idx = i;
			}
			return false;
		}
	}
	return true;
}

class Bajard_operator{

private:
	ZZ q; //Ciphertext modulus
	long ciphertext_degree; //The ciphertext degree n
	ZZ t; //Plaintext modulus
	long k_power; //Used in optimized variant
	long m_power = 0; //Used in optimized variant
	ZZ gamma; //Correction factor
	ZZ gamma_times_t; //Product of plaintext modulus and correction factor
	ZZ gamma_inv_mod_t; //Inverse of gamma modulo t, if applicable
	long log_gamma; //log_2 of gamma, used for faster modulus
	ZZ q_inv_mod_t, q_inv_mod_gamma; //Inverses of q modulo gamma and t
	unsigned int num_coeffs; //Number of RNS coefficients in q
	vector<ZZ> qi; //Ciphertext moduli
	vector<ZZ> qi_q; //q_i/q mod q_i
	vector<ZZ> q_qi; //q/q_i
	vector<vector<ZZ> > q_qi_mod_Bsk;
	ZZ q_qi_mod_mtilde;
	//vector<ZZ> B; //The extra base B = prod({m_i}) (also referred to as M)
	ZZ M; //Product of elements of B
	vector<ZZ> Bsk; //The extra base Bsk, consisting of B || m_{sk} (in that order)
	vector<ZZ> B_Bi; //B/B_i
	vector<ZZ> Bi_B; //B_i/B mod B_i
	ZZ m_tilde; //The extra modulus used in Small Montgomery Reduction
	long log_m_tilde; //log_2 of m_tilde;
	//vector<ZZ> m_tilde_inv_mod_Bsk; //Inverse of m~ mod the bases of Bsk
	//std::pair<ZZX, ZZX> secret_key; //The secret key s
	//ZZX secret_key;
	vector<ZZX> secret_key_RNS;
	//std::pair<vector<ZZX>, vector<ZZX> > relin_keys_RNS; //The relinearization key, in RNS form 
	vector<vector<ZZX> > rlk_beta, rlk_alpha; //The relin keys
	vector<ZZ> q_inv_mod_Bsk; //Modulus' inverses moduluo elements of Bsk
	ZZ M_inv_mod_msk; 
	ZZ q_inv_mod_mtilde;
	ZZ q_mod_mtilde;

	vector<ZZ> twiddle_dummies; //Used in NTT, dummy values for this evaluation


	std::ostringstream oss; //Output filestream
	duration_t overall_start, overall_end;
	duration_t FBC_dec_start, FBC_dec_end;
	duration_t FBC_Bsk_start, FBC_Bsk_end;
	duration_t FBC_floor_start, FBC_floor_end;
	duration_t decomp_start, decomp_end;


	unsigned int inv_power_shiftamt(unsigned int i){
		unsigned int k_coeffs = this->qi.size()-2;
		if(!i){
			return this->m_power - (k_coeffs+1);
		}
		else{
			return (this->m_power << (i-1)) - (k_coeffs-(i-1)+3);
		}		
	}

	//Smallest moduli
	void init_moduli_optimized(long q_numbits){
		//Old divisibility testing - not needed once you assume everything are well-chosen powers of two		ZZ q_numbits_ZZ(q_numbits);
		/*
		ZZ f_minus_one(1);
		f_minus_one <<= num_coeffs;
		ZZ result;
		assert(divide(result, q_numbits_ZZ, f_minus_one));
		*/
		k_power = q_numbits;
		//auto k_numcoeffs = num_coeffs - 2;
		assert(num_coeffs >= 2);
		q = 1;
		qi.reserve(num_coeffs);
		//unsigned int f = num_coeffs - 1;
		ZZ coeff_tmp;
		//Now start from smallest coeff
		coeff_tmp = 1;
		//long power = q_numbits >> f;
		coeff_tmp <<= m_power;
		coeff_tmp -= 1;
		qi.push_back(coeff_tmp);
		q *= qi.back();
		for(unsigned int k = 0; k < num_coeffs-1; k++){
			coeff_tmp = 1;
			//long power = q_numbits >> k;
			coeff_tmp <<= (m_power << (k));
			coeff_tmp += 1;
			qi.push_back(coeff_tmp);
			q *= qi.back();
		}

		
		//Old method
		/*
		for(unsigned int k = 1; k <= f; k++){
			coeff_tmp = 1;
			long power = q_numbits >> k;
			coeff_tmp <<= power;
			coeff_tmp += 1;
			qi.push_back(coeff_tmp);
			q *= qi.back();
		}

		coeff_tmp = 1;
		long power = q_numbits >> f;
		coeff_tmp <<= power;
		coeff_tmp -= 1;
		qi.push_back(coeff_tmp);
		q *= qi.back();
		*/

		return;
	}

	void init_moduli_default(long q_numbits){
		long coeff_size = q_numbits / num_coeffs;
		qi.reserve(num_coeffs);
		//Constructing coefficients
		ZZ primeBound(1);
		q = 1;
		primeBound <<= coeff_size;
		ZZ tmp;
		for(unsigned int i = 0; i < num_coeffs; i++){
			tmp = NextPrime(primeBound, PRIME_ERROR);
			qi.push_back(tmp);
			q *= qi.back();
			primeBound = qi.back();
			primeBound++;
		}
	}

	void init_coeff_inverses(){
		//Construct q/q_i and q_i/q mod q_i
		q_qi.resize(num_coeffs);
		qi_q.resize(num_coeffs);
		assert(qi.size() == num_coeffs);
		assert(q > 0);
		
		for(unsigned int j = 0; j < num_coeffs; j++){
			assert(qi[j] > 1);
			q_qi[j] = q/qi[j];
			//DEBUG
			/*
			cout << "q_qi[j]: " << q_qi[j] << '\n';
			cout << "q_qi[j] % qi[j]: " << q_qi[j] % qi[j] << '\n';
			cout << "qi[j]: " << qi[j] <<'\n';
			cout << "GCD: " << GCD(q_qi[j], qi[j]) << "\n\n";
			*/
			qi_q[j] = InvMod(q_qi[j] % qi[j], qi[j]);
			assert(qi_q[j] < qi[j]);
			assert(qi_q[j] < qi[j]);
		}
		
		return;
	}

	void init_mult_parms(long q_numbits, ZZ & primeBound, const ZZ & msk, unsigned int num_aux_coeffs, unsigned int optimized){
		M = 1;
		long coeff_size = q_numbits / num_coeffs;
		if(primeBound == 0){
			primeBound = 1;
			primeBound <<= coeff_size;
		}
		if((optimized & OPTIMIZE_MOD) || (optimized & OPTIMIZE_MULT)){
			num_aux_coeffs = 3;
		}
		Bsk.reserve(num_aux_coeffs);
		for(unsigned int i = 0; i < num_aux_coeffs-1; i++){
			Bsk.push_back(NextPrime(primeBound, PRIME_ERROR));
			M *= Bsk.back();
			primeBound = Bsk.back();
			primeBound++;
		}
		if(msk != 0){
			Bsk.push_back(msk);
		}
		else{
			Bsk.push_back(NextPrime(primeBound, PRIME_ERROR));
		}
		B_Bi.resize(num_aux_coeffs);
		Bi_B.resize(num_aux_coeffs);
		for(unsigned int j = 0; j < num_aux_coeffs; j++){
			B_Bi[j] = M/Bsk[j];
			Bi_B[j] = InvMod(B_Bi[j] % Bsk[j], Bsk[j]);
		}

		m_tilde = DEFAULT_M_TILDE;
		if(optimized & OPTIMIZE_MTILDE){
			assert(weight(m_tilde) == 1);
			log_m_tilde = NumBits(m_tilde);
		}
		//m_tilde_inv_mod_Bsk.reserve(num_aux_coeffs);
		q_inv_mod_Bsk.reserve(num_aux_coeffs);
		for(unsigned int i = 0; i < num_aux_coeffs; i++){
			//m_tilde_inv_mod_Bsk.push_back(InvMod(m_tilde % Bsk[i], Bsk[i]));
			q_inv_mod_Bsk.push_back(InvMod(q % Bsk[i], Bsk[i]));
		}
		q_inv_mod_mtilde = InvMod(q % m_tilde, m_tilde);

		q_qi_mod_Bsk.resize(Bsk.size());
		for(unsigned int j = 0; j < Bsk.size(); j++){
			q_qi_mod_Bsk[j].resize(num_coeffs);
			for(unsigned int i = 0; i < num_coeffs; i++){
				q_qi_mod_Bsk[j][i] = q_qi[i] % Bsk[j];
			}
		}
	}


	void init_secret_key(){
		/*
		ZZX secret_key;
		error_dist(secret_key, ciphertext_degree);
		*/
		//Put the secret key in RNS form
		secret_key_RNS.resize(num_coeffs);
		for(unsigned int i = 0; i < num_coeffs; i++){
			//secret_key_RNS[i] = secret_key;
			//long degree = deg(secret_key_RNS[i]);
			for(long d = 0; d <= this->ciphertext_degree; d++){
				long tmp = (rand() % 3) - 1;
				if(tmp < 0){
					SetCoeff(secret_key_RNS[i], d, qi[i]+tmp);
				}
				else{
					SetCoeff(secret_key_RNS[i], d, tmp);
				}
				/*
				if(secret_key_RNS[i][d] < 0){
					secret_key_RNS[i][d] = qi[i] - secret_key_RNS[i][d];
				}
				secret_key_RNS[i][d] %= qi[i];
				*/
			}
		}
	}


	void init_relin_key(){
		vector<ZZX> e_error, a_public; //The error and part of the public key, used in generating the relinearization key
		e_error.resize(num_coeffs);
		a_public.resize(num_coeffs);
		rlk_beta.resize(num_coeffs);
		rlk_alpha.resize(num_coeffs);
		ZZX tmp;
		ZZX poly_mod;
		//poly_mod.SetLength(this->ciphertext_degree+1);
		SetCoeff(poly_mod, this->ciphertext_degree+1, 1);
		//poly_mod[this->ciphertext_degree] = 1;
		poly_mod[0] = 1;
		//cout << "Modulus deg: " << deg(poly_mod) << endl;
		ZZX sk_sq;
		for(unsigned int j = 0; j < num_coeffs; j++){
			if(deg(secret_key_RNS[j]) >= deg(poly_mod)){
				cerr << "s deg: " << deg(secret_key_RNS[j]) << endl;
				cerr << "mod deg: " << deg(poly_mod) << endl;
				assert(0 && "Bad poly. args");
			}
			sk_sq = MulMod(secret_key_RNS[j], secret_key_RNS[j], poly_mod);
			error_dist(e_error[j], this->ciphertext_degree, qi[j]);
			unif_vec(a_public[j], this->ciphertext_degree, qi[j]);
			rlk_beta[j].resize(num_coeffs);
			rlk_alpha[j].resize(num_coeffs);
			for(unsigned int i = 0; i < num_coeffs; i++){
				rlk_alpha[j][i] = mod_poly(a_public[j], qi[i]);
				tmp = (qi[j]*q_qi[j])*sk_sq 
					- MulMod(rlk_alpha[j][i], secret_key_RNS[j], poly_mod) + e_error[j];
				rlk_beta[j][i] = mod_poly(tmp, qi[i]);
			}
		}
		//Kill unused memory
		for(auto & x : a_public){
			x.kill();
		}
		for(auto & x : e_error){
			x.kill();
		}
		return;
	}

std::pair<ZZX, ZZX> FastBConv_decryption(const vector<ZZX> & x, unsigned int optimized) {
#ifdef DEBUG
		assert(x.size() == qi.size());
		assert(x.size() == num_coeffs);
#endif		
		std::pair<ZZX, ZZX> ret;
		/*
		ret.first = 1;
		ret.second = 1;
		*/
		
		ret.first.SetLength(ciphertext_degree+1);
		ret.second.SetLength(ciphertext_degree+1);

		ZZ sum, term;
		ZZ minuend;
		//ZZ power;
		FBC_dec_start = high_resolution_clock::now();
		//long deg = max_poly_degMax(x);

		vector<ZZX> tmp_transition(num_coeffs);
		NTL_EXEC_RANGE(num_coeffs, first, last)
		for(long i = first; i < last; i++){
			long degree_x = deg(x[i]);
			if(degree_x == -1){
				clear(tmp_transition[i]);
				return;
			} 
			tmp_transition[i].SetLength(degree_x+1);
			long g = this->m_power << (i ? i-1 : 0);
			long y = i ? (this->m_power << (i-1)) - (qi.size()-i+2) : this->m_power - (qi.size()-1);
			
			for(long d = 0; d <= degree_x; d++){
				if((optimized & OPTIMIZE_MULT) && (optimized & OPTIMIZE_MOD)){
#ifdef DEBUG
					assert(in_range(coeff(x[i], d), qi[i]));
#endif							
					if(!i){
						SetCoeff(tmp_transition[i], d, shift_mod_min1(coeff(x[i], d), y, g));
					}
					else{
						SetCoeff(tmp_transition[i], d, shift_mod_plus1(coeff(x[i], d), y, g));
					}

#ifdef DEBUG
					assert(in_range(tmp_transition[i][d], qi[i]));
#endif		

					if(optimized & OPTIMIZE_GAMMA){
						if(!i){
							tmp_transition[i][d] = shift_mod_min1(tmp_transition[i][d], log_gamma, g);
						}
						else{
							tmp_transition[i][d] = shift_mod_plus1(tmp_transition[i][d], log_gamma, g);
						}
					}
				}
				else{
					if(optimized & OPTIMIZE_MULT){
						//long power = ((k_power >> (i+1))-(i+1));
						SetCoeff(tmp_transition[i], d, coeff(x[i], d) << y);
					}
					else{
						SetCoeff(tmp_transition[i], d, coeff(x[i], d) * qi_q[i]);
					}

					if(optimized & OPTIMIZE_MOD){
						if(!i){
							tmp_transition[i][d] = mod_min_one(tmp_transition[i][d], g, qi[i]);
						}
						else{
							tmp_transition[i][d] = mod_plus_one(tmp_transition[i][d], g, qi[i]);
						}
					}
					else{
						tmp_transition[i][d] %= qi[i];
					}

					if((optimized & OPTIMIZE_GAMMA)){
						tmp_transition[i][d] <<= log_gamma;
					}
					else{
						tmp_transition[i][d] *= gamma;
					}
					if(optimized & OPTIMIZE_MOD){
						if(!i){
							tmp_transition[i][d] = mod_min_one(tmp_transition[i][d], g, qi[i]);
						}
						else{
							tmp_transition[i][d] = mod_plus_one(tmp_transition[i][d], g, qi[i]);
						}
					}
				}
			}
		}
		NTL_EXEC_RANGE_END

		for(unsigned int i = 0; i < tmp_transition.size(); i++){
			long tmp_deg = deg(tmp_transition[i]);
			NTL_EXEC_RANGE(tmp_deg+1, first, last)
			for(long d = first; d < last; d++){
				ret.first[d] += tmp_transition[i][d]*(q_qi[i] % t);
				ret.first[d] %= t;
				ret.second[d] += tmp_transition[i][d]*(q_qi[i] % gamma);
				ret.second[d] %= gamma;
			}
			NTL_EXEC_RANGE_END
		}

		FBC_dec_end = high_resolution_clock::now();
		return ret;
	}

/*
	void find_s(ZZX & arg, const ZZ & mcand, const ZZ & modulus){
		//long deg = degree(arg);
		for(long i = 0; i <= ciphertext_degree; i++){
			arg[i] *= -mcand;
			arg[i] %= modulus;
			if(arg[i] < 0){
				arg[i] = -arg[i];
			}
		}
	}
*/

	//Assumes all arguments are same size
	//Evaluates ciphertext on secret key
	vector<ZZX> ctext_in_secret_key(const vector<ZZX> & ct0, const vector<ZZX> & ct1, unsigned int optimized) const {
#ifdef DEBUG
		assert(ct0.size() == ct1.size());
		assert(ct0.size() == this->secret_key_RNS.size());
		assert(ct0.size() == this->qi.size());
		assert(ct0.size() == num_coeffs);
#endif		
		vector<ZZX> ret = ct0;	
		NTL_EXEC_RANGE(num_coeffs, first, last)
		//1 per thread - expensive, but better than race conditions
		ZZX tmp;
		for(long i = first; i < last; i++){
#ifdef DEBUG
			assert(in_range(ct0[i], this->qi[i]));
			assert(in_range(ret[i], this->qi[i]));
			assert(in_range(ct1[i], this->qi[i]));
			assert(in_range(this->secret_key_RNS[i], this->qi[i]));
#endif				
			clear(tmp);
			if(!((optimized & OPTIMIZE_MOD) && (optimized & OPTIMIZE_MOD))){
				tmp = poly_mult_NTT_unoptimized(ct1[i], this->secret_key_RNS[i], this->ciphertext_degree+1,
				 this->twiddle_dummies, this->twiddle_dummies, this->qi[i]);
#ifdef DEBUG
				assert(deg(tmp) <= this->ciphertext_degree);
				long idx = -1;
				bool r = in_range(tmp, this->qi[i], &idx);
				if(!r){
					cerr << "ERROR out of range tmp " << tmp[idx]
						 << " qi " << qi[i] << " i " << i
						 << " idx " << idx << endl; 
				}
				assert(r);
#endif		
			}
			else{
				long g = this->m_power << (i ? i-1 : 0);
				tmp = poly_mult_NTT_optimized(ct1[i], this->secret_key_RNS[i], this->ciphertext_degree+1,
				 this->twiddle_dummies, this->twiddle_dummies, this->qi[i], g, i);
#ifdef DEBUG
				assert(deg(tmp) <= this->ciphertext_degree);
				long idx = -1;
				bool r = in_range(tmp, this->qi[i], &idx);
				if(!r){
					cerr << "ERROR out of range tmp " << tmp[idx]
						 << " qi " << qi[i] << " i " << i
						 << " idx " << idx << endl; 
				}
				assert(r);
#endif	
			}
#ifdef DEBUG
			assert(i >= 0);
			assert(i < num_coeffs);
			assert(in_range(tmp, this->qi[i]));
			assert(in_range(ret[i], this->qi[i]));
#endif			
			poly_add_inplace(ret[i], tmp, this->qi[i]);
			//ret[i] = poly_add_simplemod(ret[i], tmp, this->qi[i]);

#ifdef DEBUG
			long idx = -1;
			assert(in_range(ret[i], this->qi[i], &idx));
#endif			
		}
		NTL_EXEC_RANGE_END
		return ret;
	}

	//End of decryption, multiplication hereafter

	//Converts, multiplying by m_tilde
	//Assumes all arguments in regular modulus
	vector<ZZX> FastBConv_Bsk_mtilde(const vector<ZZX> & x, unsigned int optimized) {
		vector<ZZX> ret(Bsk.size() + 1);
		for(auto & component : ret){
			component.SetLength(ciphertext_degree+1);
#ifdef DEBUG			
			if(deg(component) == -1){
				cerr << "normalize is wrong too" << endl;
			}
#endif			
		}

		vector<ZZX> tmp_transition(x.size());
		for(size_t i = 0; i < tmp_transition.size(); i++){
			long g = this->m_power << (i ? i-1 : 0);
			long y = i ? (this->m_power << (i-1)) - (qi.size()-i+2) : this->m_power - (qi.size()-1);
			long component_deg = deg(x[i]);
			if(component_deg < 0){
				continue;
			}
			tmp_transition[i].SetLength(component_deg+1);
			for(long d = 0; d <= component_deg; d++){
				if((optimized & OPTIMIZE_MULT) && (optimized & OPTIMIZE_MOD)){
					if(!i){
						tmp_transition[i][d] = shift_mod_min1(x[i][d], y, g);
					}
					else{
						tmp_transition[i][d] = shift_mod_plus1(x[i][d], y, g);
					}

					if(optimized & OPTIMIZE_MTILDE){
						if(!i){
							tmp_transition[i][d] = shift_mod_min1(x[i][d], log_m_tilde, g);
						}
						else{
							tmp_transition[i][d] = shift_mod_plus1(x[i][d], log_m_tilde, g);
						}
					}
				}
				else{
					if(optimized & OPTIMIZE_MULT){
						//long power = ((k_power >> (i+1))-(i+1));
						LeftShift(tmp_transition[i][d], x[i][d], y);
						//tmp_transition[i][d] <<= y;
					}
					else{
						mul(tmp_transition[i][d], x[i][d], qi_q[i]);
						//tmp_transition[i][d] *= qi_q[i];
					}

					if(optimized & OPTIMIZE_MOD){
						if(!i){
							tmp_transition[i][d] = mod_min_one(tmp_transition[i][d], g, qi[i]);
						}
						else{
							tmp_transition[i][d] = mod_plus_one(tmp_transition[i][d], g, qi[i]);
						}
					}
					else{
						tmp_transition[i][d] %= qi[i];
					}

					if((optimized & OPTIMIZE_MTILDE)){
						tmp_transition[i][d] <<= log_m_tilde;
					}
					else{
						tmp_transition[i][d] *= m_tilde;
					}
					if(optimized & OPTIMIZE_MOD){
						if(!i){
							tmp_transition[i][d] = mod_min_one(tmp_transition[i][d], g, qi[i]);
						}
						else{
							tmp_transition[i][d] = mod_plus_one(tmp_transition[i][d], g, qi[i]);
						}
					}
				}
			}
		}

		//Now iterate over extbases
		ZZ mod_tmp;
		for(size_t j = 0; j < Bsk.size(); j++){
			for(size_t i = 0; i < this->num_coeffs; i++){
				long degree = deg(tmp_transition[i]);
				if(degree < 0){
					continue;
				}
				clear(mod_tmp);
				for(long d = 0; d < degree; d++){
					mul(mod_tmp, q_qi_mod_Bsk[j][i], tmp_transition[i][d]);
					mod_tmp %= Bsk[j];
					ret[j][d] += mod_tmp;
				}
			}
			long degree_result = deg(ret[j]);
			if(degree_result < 0){
				continue;
			}
			for(long d = 0; d < degree_result; d++){
				ret[j][d] %= Bsk[j];
			}
		}
		//Handle Bsk
		for(size_t i = 0; i < this->num_coeffs; i++){
			long degree = deg(tmp_transition[i]);
			if(degree < 0){
				continue;
			}
			for(long d = 0; d < degree; d++){
				mul(mod_tmp, this->q_qi_mod_mtilde, tmp_transition[i][d]);
				mod_tmp %= this->m_tilde;
				ret.back()[d] += mod_tmp;
			}
		}
		long degree_result = deg(ret.back());
		if(degree_result >= 0){
			for(long d = 0; d < degree_result; d++){
				ret.back()[d] %= this->m_tilde;
			}
		}
		
		return ret;
	}

	inline ctext FastBConv_Bsk_mtilde(const ctext & arg, unsigned int optimized) {
		ctext ret;
		FBC_Bsk_start = high_resolution_clock::now();
		ret.first = FastBConv_Bsk_mtilde(arg.first, optimized);
		ret.second = FastBConv_Bsk_mtilde(arg.second, optimized);
		FBC_Bsk_end = high_resolution_clock::now();
		return ret;
	}

	vector<ZZX> SmRq(const vector<ZZX> & c_double) {
#ifdef DEBUG
		assert(c_double.size() == Bsk.size()+1);
#endif
		//ZZX q_r_mtilde = -c_double.back();
		ZZX r_mtilde = poly_negate(c_double.back(), m_tilde);
		poly_scale_inplace(r_mtilde, q_inv_mod_mtilde, m_tilde);
		//q_r_mtilde *= q_inv_mod_mtilde;
		//Take out the centered part
		//cmod_inplace(q_r_mtilde, m_tilde);
		
		//q*cmod((-c_double.back())*q_inv_mod_mtilde, m_tilde);
		long ext_base_size = Bsk.size();
		//vector<ZZX> c_prime(ext_base_size);
		vector<ZZX> c_prime(ext_base_size);
		/*
		c_prime.reserve(ext_base_size);
		c_prime.resize(ext_base_size);
		for(long i = 0; i < ext_base_size; i++){
			//ZZX tmp;
			//c_prime.push_back(tmp);
			c_prime[i] = 0;
		}
		*/
		//long max_deg = max_poly_deg(c_double);
		//Calculate c`_{m~}
		//NTL_EXEC_RANGE(ext_base_size, first, last)
		//for(long i = first; i < last; i++){
		for(long i = 0; i < ext_base_size; i++){
			ZZX tmp = poly_mult_add(c_double[i], r_mtilde, q_inv_mod_Bsk[i], Bsk[i]);
			c_prime[i] = tmp;
		}
		//NTL_EXEC_RANGE_END

		return c_prime;
	}

	inline ctext SmRq(const ctext & arg) {
		ctext ret;
		ret.first = SmRq(arg.first);
		ret.second = SmRq(arg.second);
		return ret;
	}

	inline ZZX poly_square(const ZZX & arg) const {

		ZZX ret = arg*arg;
		ZZX poly_modulus;
		poly_modulus[0] = 1;
		poly_modulus[ciphertext_degree] = 1;
		ret %= poly_modulus;

		return ret;
	}

	void relin(vector<ZZX> & ct0, vector<ZZX> & ct1, const vector<ZZX> & ct2, unsigned int optimized) const {
#ifdef DEBUG
		assert(ct0.size() == num_coeffs);
#endif		
		//ZZX sum_b, sum_a, tmp_b, tmp_a;
		ZZX sum, tmp;
		ZZX sum_a, sum_b, tmp_a, tmp_b;
		long degree = ciphertext_degree;

		for(long j = 0; j < this->num_coeffs; j++){
			clear(sum_a);
			clear(sum_b);
			long g = this->m_power << (j ? j-1 : 0);
			for(long i = 0; i < this->num_coeffs; i++){
				if((optimized & OPTIMIZE_MOD) && (optimized & OPTIMIZE_MULT)){
					tmp_a = poly_inner_optimized(rlk_beta[i][j], ct2[j], qi[j], j, g);
					tmp_b = poly_inner_optimized(rlk_alpha[i][j], ct2[j], qi[j], j, g);
				} else {
					tmp_b = poly_inner_unoptimized(rlk_beta[i][j], ct2[j], qi[j]);
					tmp_a = poly_inner_unoptimized(rlk_alpha[i][j], ct2[j], qi[j]);
					//tmp_a = inner_product(rlk_alpha[i][j], ct2[j]);
					//mod_inplace(tmp, qi[i]);
					//mod_inplace(tmp_a, qi[i]);
				}
#ifdef DEBUG
				assert(max(deg(sum), deg(tmp)) <= this->ciphertext_degree);
				for(long d = 0; d <= degree; d++){
					assert(coeff(tmp_a, d) >= 0);
					assert(coeff(tmp_a, d) < qi[j]);
					assert(coeff(tmp_b, d) >= 0);
					assert(coeff(tmp_b, d) < qi[j]);
				}
				//cerr << "sum[0]: " << coeff(sum, 0) << endl;
#endif		
				poly_add_inplace(sum_b, tmp_b, qi[j]);
				poly_add_inplace(sum_a, tmp_a, qi[j]);
			}
			poly_add_inplace(ct0[j], tmp_b, qi[j]);
			poly_add_inplace(ct1[j], tmp_a, qi[j]);
		}
		return;
	}

	//Simple fast flooring from base conv.
	//Converts from q to Bsk
	//All arguments should be in the regular modulus
	vector<ZZX> FastBConv_floor(const vector<ZZX> & x,unsigned int optimized) {
	#ifdef DEBUG
		assert(x.size() == qi.size());
		assert(this->num_coeffs == x.size());
	#endif		
		vector<ZZX> ret(Bsk.size());
		for(auto & component : ret){
			component.SetLength(ciphertext_degree+1);
		}

		vector<ZZX> tmp_transition(x.size());
		FBC_floor_start = high_resolution_clock::now();
		NTL_EXEC_RANGE(this->num_coeffs, first, last)
		for(long i = first; i < last; i++){
			long g = this->m_power << (i ? i-1 : 0);
			long y = i ? (this->m_power << (i-1)) - (qi.size()-i+2) : this->m_power - (qi.size()-1);
			long component_deg = deg(x[i]);
			if(component_deg < 0){
				continue;
			}
			tmp_transition[i].SetLength(component_deg+1);
			for(long d = 0; d <= component_deg; d++){
#ifdef DEBUG
				assert(in_range(x[i][d], qi[i]));
#endif				
				if((optimized & OPTIMIZE_MULT) && (optimized & OPTIMIZE_MOD)){
					if(!i){
						SetCoeff(tmp_transition[i], d, shift_mod_min1(x[i][d], y, g));
					}
					else{
						SetCoeff(tmp_transition[i], d, shift_mod_plus1(x[i][d], y, g));
					}

					if(optimized & OPTIMIZE_MTILDE){
						if(!i){
							tmp_transition[i][d] = shift_mod_min1(x[i][d], log_m_tilde, g);
						}
						else{
							tmp_transition[i][d] = shift_mod_plus1(x[i][d], log_m_tilde, g);
						}
					}
				}
				else{
					if(optimized & OPTIMIZE_MULT){
						//long power = ((k_power >> (i+1))-(i+1));
						LeftShift(tmp_transition[i][d], x[i][d], y);
						//tmp_transition[i][d] <<= y;
					}
					else{
						mul(tmp_transition[i][d], x[i][d], qi_q[i]);
						//tmp_transition[i][d] *= qi_q[i];
					}

					if(optimized & OPTIMIZE_MOD){
						if(!i){
							tmp_transition[i][d] = mod_min_one(tmp_transition[i][d], g, qi[i]);
						}
						else{
							tmp_transition[i][d] = mod_plus_one(tmp_transition[i][d], g, qi[i]);
						}
					}
					else{
						tmp_transition[i][d] %= qi[i];
					}

					if((optimized & OPTIMIZE_MTILDE)){
						tmp_transition[i][d] <<= log_m_tilde;
					}
					else{
						tmp_transition[i][d] *= m_tilde;
					}
					if(optimized & OPTIMIZE_MOD){
						if(!i){
							tmp_transition[i][d] = mod_min_one(tmp_transition[i][d], g, qi[i]);
						}
						else{
							tmp_transition[i][d] = mod_plus_one(tmp_transition[i][d], g, qi[i]);
						}
					}
				}
			}
		}
		NTL_EXEC_RANGE_END

		for(long i = 0; i < num_coeffs; i++){
			//Now assign final product
			long ext_base_size = Bsk.size();
			NTL_EXEC_RANGE(ext_base_size, first, last)
			for(long j = first; j < last; j++){
				//SetCoeff(ret[j], d, sum % Bsk[i]);
				//mod_inplace(ret[j], Bsk[j]);
				long degree = deg(tmp_transition[i]);
				ret[j].SetLength(degree+1);
				
				for(long d = 0; d <= degree; d++){
					SetCoeff(ret[j], d, coeff(ret[j], d) + (coeff(tmp_transition[i], d) * q_qi_mod_Bsk[j][i]));
					//ret[j][d] += tmp_transition[i][d] * q_qi_mod_Bsk[i][j];
					ret[j][d] %= Bsk[j];
				}
				
			}
			NTL_EXEC_RANGE_END
		}
		FBC_floor_end = high_resolution_clock::now();
		return ret;
	}

	vector<ZZX> fastRNSFloor(const vector<ZZX> & x_q, const vector<ZZX> & x_Bsk, unsigned int optimized) {
#ifdef DEBUG
		assert(x_q.size() == qi.size());
		for(size_t i = 0; i < x_q.size(); i++){
			long degree = deg(x_q[i]);
			for(long d = 0; d <= degree; d++){
				assert(in_range(x_q[i][d], qi[i]));
			}
		}
#endif		
		vector<ZZX> fastbconv_result = FastBConv_floor(x_q, optimized); //Same size as Bsk
		long ext_base_size = Bsk.size();
		NTL_EXEC_RANGE(ext_base_size, first, last)
		for(long i = first; i < last; i++){
			//long deg = degree(fastbconv_result[i]);
			for(long d = 0; d <= ciphertext_degree; d++){
				fastbconv_result[i][d] = -fastbconv_result[i][d];
				fastbconv_result[i][d] += x_Bsk[i][d];
				fastbconv_result[i][d] *= q_inv_mod_Bsk[i];
				fastbconv_result[i][d] %= Bsk[i];
				if(fastbconv_result[i][d] < 0){
					fastbconv_result[i][d] += Bsk[i];
				}
			}
		}
		NTL_EXEC_RANGE_END
		return fastbconv_result;
	}

	//x is in base Bsk
	inline ZZX alpha_sk(const vector<ZZX> & x) {
#ifdef DEBUG
		assert(x.size() == Bsk.size());
#endif		
		long retsize = ciphertext_degree+1;
		ZZX ret;
		ret.SetLength(retsize);
		/*
		NTL_EXEC_RANGE(retsize, first, last)
		for(long i = first; i < last; i++){
			clear(ret[i]);
		}
		NTL_EXEC_RANGE_END
		*/

		ZZ tmp;
		const ZZ & msk = Bsk.back();
		for(long d = 0; d <= ciphertext_degree; d++){
			//Do a fast base conversion
			//ret[d] = 0;
			clear(ret[d]);
			for(size_t i = 0; i < Bsk.size()-1; i++){
				//clear(tmp);
				tmp = coeff(x[i], d);
				tmp *= Bi_B[i];
				tmp %= Bsk[i];
				tmp *= B_Bi[i];
				ret[d] += tmp;
			}
			//Do the mod to m_sk at the end
			ret[d] %= msk;
			if(ret[d] < 0){
				ret[d] += msk;
			}
			ret[d] -= x.back()[d];
			ret[d] *= M_inv_mod_msk;
			cmod_inplace(ret[d], msk);
		}
		return ret;
	}

	vector<ZZX> FastBConv_q(const vector<ZZX> & x){
		//long deg = max_poly_deg(x);
		vector<ZZX> ret(qi.size());
		for(auto & vec : ret){
			vec.SetLength(ciphertext_degree+1);
		}
		ZZ sum(0);
		static ZZ tmp;
		for(long d = 0; d <= ciphertext_degree; d++){
			clear(sum);
			for(size_t i = 0; i < x.size(); i++){
				clear(tmp);
				tmp = x[i][d];
				tmp *= Bi_B[i];
				tmp %= Bsk[i];
				tmp *= B_Bi[i];
				sum += tmp;
			}
			for(size_t j = 0; j < ret.size(); j++){
				//SetCoeff(ret[j], d, sum % qi[j]);
				SetCoeff(ret[j], d, sum);
				ret[j][d] %= qi[j];
				if(ret[j][d] < 0){
					ret[j][d] += qi[j];
				}
			}
		}
		return ret;
	}

	//Assume x is given in base Bsk
	vector<ZZX> FastBconvSK(const vector<ZZX> & x) {
#ifdef DEBUG
		assert(x.size() == Bsk.size());
#endif		
		ZZX a_sk = alpha_sk(x);
		for(long i = 0; i <= deg(a_sk); i++){
			a_sk[i] *= (M % Bsk.back());
		}
		vector<ZZX> q_result = FastBConv_q(x);
		for(size_t i = 0; i < q_result.size(); i++){
			//long deg = degree(q_result[i]);
			for(long d = 0; d <= ciphertext_degree; d++){
				q_result[i][d] -= a_sk[i];
				q_result[i][d] %= qi[i];
			}
		}
		//a_sk.kill();
		return q_result;
	}


public:

	const static long WORD_BITS_PER_COEFF = 60;
	const static long PRIME_ERROR = 40;
	const static long DEFAULT_GAMMA = 1 << 8;
	const static long DEFAULT_M_TILDE = 1 << 16;
	const static int RAND_SEED = 5;

	const static int OPTIMIZE_MULT = 0x1;
	const static int OPTIMIZE_MOD = 0x2;
	const static int OPTIMIZE_GAMMA = 0x4;
	const static int OPTIMIZE_MTILDE = 0x8;

	long poly_deg(){
		return this->ciphertext_degree;
	}

	void init(long q_numbits, long poly_deg, 
		const ZZ & plain_modulus, unsigned int optimized, bool mult_parms = true){

		assert(q_numbits > 0);
		assert(poly_deg > 0);
		assert(weight(poly_deg+1) == 1); //Check that the poly. mod. deg. is a power of two

		/*
		if(num_coeffs_in){
			num_coeffs = num_coeffs_in;
		}
		*/
		//this->m_power = m_power_in;
		//Just assume log2(q) and MAX_BITS_PER_COEFF are powers of two...
		if (optimized & (OPTIMIZE_MOD | OPTIMIZE_MULT)){
			this->num_coeffs = 1;
			long q_tmp = q_numbits;
			while(q_tmp > this->WORD_BITS_PER_COEFF){
				q_tmp /= 2;
				num_coeffs++;
			}
			this->m_power = q_tmp;
			/*
			ZZ q_nb_ZZ(q_numbits);
			//DEBUG
			cout << "q: " << q_nb_ZZ << " q_bits: " << ceil(log(q_nb_ZZ)) << endl;
			ZZ max_bits_ZZ(MAX_BITS_PER_COEFF);
			long q_log = ceil(log(q_nb_ZZ));
			long maxbits_log = ceil(log(max_bits_ZZ));
			*/
			/*
			long q_log = NumBits(q_numbits);
			long maxbits_log = NumBits(WORD_BITS_PER_COEFF);
			if(q_log > maxbits_log){
				num_coeffs = q_log - maxbits_log + 1;
			}
			else{
				num_coeffs = 1;
			}
			*/
		}	
		else{
			ZZ q_bits_ZZ(q_numbits);
			ZZ div_q;
			long rem = DivRem(div_q, q_bits_ZZ, WORD_BITS_PER_COEFF);
			conv(num_coeffs, div_q);
			if(rem){
				num_coeffs++;
			}
		}
		
		ciphertext_degree = poly_deg;
		t = plain_modulus;
		q = 1;
		ZZ primeBound;
		if(!(optimized & (OPTIMIZE_MOD | OPTIMIZE_MULT))){
			init_moduli_default(q_numbits);
			//Last modulus is the largest
			primeBound = qi.back() + 1;
		}
		else{
			init_moduli_optimized(q_numbits);
			//Set to 0 to choose Bsk as mormal
			primeBound = 0;
		}
		init_coeff_inverses();
		//Construct parms needed for decryption
		gamma = DEFAULT_GAMMA;
		gamma_times_t = gamma*t;
		if(GCD(gamma, t) == 1){
			gamma_inv_mod_t = InvMod(gamma % t, t);
		}
		else{
			gamma_inv_mod_t = 0;
		}
		log_gamma = NumBits(gamma);
		q_inv_mod_t = InvMod(q % t, t);
		q_inv_mod_gamma = InvMod(q % gamma, gamma);
		if(optimized & OPTIMIZE_GAMMA){
			assert(weight(gamma) == 1);
		}
		init_secret_key();
		if(!mult_parms){
			return;
		}
		ZZ msk;
		set(msk);
		msk <<= (WORD_BITS_PER_COEFF/2);
		msk = NextPrime(msk, PRIME_ERROR);
		ZZ tmp;
		while(InvModStatus(tmp, msk, q)){
			msk = NextPrime(msk, PRIME_ERROR);
		}
		init_mult_parms(q_numbits, primeBound, msk, num_coeffs+1, optimized);
		init_relin_key();
		srand(RAND_SEED);
		
		//Clear memory
		q.kill();

		//Init twiddle factors
		this->twiddle_dummies.resize(this->ciphertext_degree+1);
		for(long i = 0; i <= this->ciphertext_degree; i++){
			this->twiddle_dummies[i] = i;
		}

		return;
	}

	ctext random_ciphertext(){
		ctext ret;
		ret.first.resize(num_coeffs);
		ret.second.resize(num_coeffs);
		for(unsigned int i = 0; i < num_coeffs; i++){
			unif_vec(ret.first[i], ciphertext_degree, qi[i]);
			unif_vec(ret.second[i], ciphertext_degree, qi[i]);	
			if((deg(ret.first[i]) == -1) 
				|| (deg(ret.second[i]) == -1)){
				cerr << "Something's wrong with unif_vec" << endl;
			}		
		}
		return ret;
	}

	
	

	ZZX Bajard_decrypt(const vector<ZZX> & ct0, const vector<ZZX> & ct1, unsigned int optimized){
#ifdef DEBUG
		assert(ct0.size() == ct1.size());
		assert(this->secret_key_RNS.size() == ct0.size());
		assert(ct0.size() == qi.size());
#endif		
		overall_start = high_resolution_clock::now();

		vector<ZZX> ctext_evaluated = ctext_in_secret_key(ct0, ct1, optimized);
		//centered_to_regular_inplace(ctext_evaluated, qi);
		//Convert all coefficients to (positive) mod before FastBConv
		//No longer needed - everything is in regular mod. at this point
		/*
		for(size_t i = 0; i < ctext_evaluated.size(); i++){
			for(long d = 0; d <= ciphertext_degree; d++){
				ctext_evaluated[i][d] %= qi[i];
				if(ctext_evaluated[i][d] < 0){
					ctext_evaluated[i][d] += qi[i];
				}
			}
		}
		*/
		//FBC_start = high_resolution_clock::now();
		std::pair<ZZX, ZZX> s_t_gamma = FastBConv_decryption(ctext_evaluated, optimized);
		//FBC_end = high_resolution_clock::now();
		ZZX & s_tilde_gamma = s_t_gamma.second;
		cmod_inplace(s_tilde_gamma, gamma);
		s_t_gamma.first -= s_tilde_gamma;
		if(optimized & OPTIMIZE_GAMMA){
			long gamma_deg = deg(s_t_gamma.first);
			for(long d = 0; d <= gamma_deg; d++){
				//Supposed to be shifting coeffs, not polynomial...
				s_t_gamma.first[d] >>= log_gamma;
			}
		}
		else{
			s_t_gamma.first *= gamma_inv_mod_t;
		}
		
		cmod_inplace(s_t_gamma.first, t);

		overall_end = high_resolution_clock::now();

		return s_t_gamma.first;
	}

	inline ZZX Bajard_decrypt(const ctext & ct, unsigned int optimized){
		return Bajard_decrypt(ct.first, ct.second, optimized);
	}

	void print_moduli(std::ostream & os){
		os << "#Moduli sizes:\n";
		for(const auto & modulus : qi){
			os << '#' << NumBits(modulus) << '\n';
		}
		os << '\n';
		os << "#Aux. moduli sizes:\n";
		for(const auto & aux_mod : Bsk){
			cout <<  '#' << NumBits(aux_mod) << '\n';
		}
		os << '\n';
	}

#define DECRYPT 1
#define MULT 0	

	void write_timing(bool decryption, unsigned int optimized, bool verbose = true){
		std::string opt_str = optimized ? "_opt" : "";
		if(optimized & OPTIMIZE_MULT){
			opt_str += "_mult";
		}
		if(optimized & OPTIMIZE_MOD){
			opt_str += "_mod";
		}
		if(!verbose){
			opt_str = "";
		}
		if(decryption == DECRYPT){
			oss << "dec_overall" << opt_str << ' ' << duration_cast<chrono::nanoseconds>(overall_end-overall_start).count() << '\n';
			oss << "dec_FastBConv" << opt_str << ' ' << duration_cast<chrono::nanoseconds>(FBC_dec_end-FBC_dec_start).count() << '\n';
		}
		else{
			oss << "mult_overall" << opt_str << ' ' << duration_cast<chrono::nanoseconds>(overall_end-overall_start).count() << '\n';
			//Group both types of FastBConv in the same category
			oss << "mult_FastBConv" << opt_str << ' ' << duration_cast<chrono::nanoseconds>(FBC_Bsk_end-FBC_Bsk_start).count() << '\n';
			oss << "mult_FastBConv_floor" << opt_str << ' ' << duration_cast<chrono::nanoseconds>(FBC_floor_end-FBC_floor_start).count() << '\n';
			//oss << "mult_decomp" << opt_str << ' ' << duration_cast<chrono::nanoseconds>(decomp_end-decomp_start).count() << '\n';
		}
		return;
	}

	void output_stream(std::ostream & os){
		os << oss.str();
		return;
	}
	

ctext Bajard_multiply(ctext & ct1, ctext & ct2, unsigned int optimized) {
		overall_start = high_resolution_clock::now();
		//Step 0
		ctext ct1_Bsk_mtilde = FastBConv_Bsk_mtilde(ct1, optimized);
		ctext ct2_Bsk_mtilde = FastBConv_Bsk_mtilde(ct2, optimized);
		//Step 1
		ctext ct1_Bsk = SmRq(ct1_Bsk_mtilde);
		ctext ct2_Bsk = SmRq(ct2_Bsk_mtilde);

		//Clear memory
		/*
		for(auto & x : ct1_Bsk_mtilde.first){
			x.kill();
		}
		for(auto & x : ct1_Bsk_mtilde.second){
			x.kill();
		}
		for(auto & x : ct2_Bsk_mtilde.first){
			x.kill();
		}
		for(auto & x : ct2_Bsk_mtilde.second){
			x.kill();
		}
		*/

		//Step 2
		vector< vector<ZZX> > ct_star_q;
		if((optimized & OPTIMIZE_MOD) && (optimized & OPTIMIZE_MULT)){
			ct_star_q = star_optimized(ct1, ct2, qi, this->m_power, this->ciphertext_degree+1, this->twiddle_dummies);
		}
		else{
			ct_star_q = star_ordinary(ct1, ct2, qi, this->ciphertext_degree+1, this->twiddle_dummies);
		}
		
		//Clear memory
		/*
		for(auto & x : ct1.first){
			x.kill();
		}
		for(auto & x : ct2.first){
			x.kill();
		}
		for(auto & x : ct1.second){
			x.kill();
		}
		for(auto & x : ct2.second){
			x.kill();
		}
		*/
		vector< vector<ZZX> > ct_star_Bsk = star_ordinary(ct1_Bsk, ct2_Bsk, Bsk, this->ciphertext_degree+1, this->twiddle_dummies);

		//Clear memory
		/*
		for(auto & x : ct1_Bsk.first){
			x.kill();
		}
		for(auto & x : ct1_Bsk.second){
			x.kill();
		}
		for(auto & x : ct2_Bsk.first){
			x.kill();
		}
		for(auto & x : ct2_Bsk.second){
			x.kill();
		}
		*/
#ifdef DEBUG
		assert(ct_star_q.size() == 3);
		assert(ct_star_Bsk.size() == 3);
#endif		
		//Multiply ciphertexts by t
		ZZ modulus;
		for(auto & ct_elt : ct_star_q){
			for(size_t i = 0; i < ct_elt.size(); i++){
				if((optimized & OPTIMIZE_MULT) && (optimized & OPTIMIZE_MOD)){
					long g = this->m_power << (i ? i-1 : 0);
					set(modulus);
					modulus <<= g;
					if(i){
						modulus++;
					}
					else{
						modulus--;
					}
					poly_scale_optimized(ct_elt[i], t, i, g, modulus);
				}
				else{
					poly_scale_unoptimized(ct_elt[i], t, qi[i]);
				}
				
			}
		}
		for(auto & ct_elt : ct_star_Bsk){
			for(size_t i = 0; i < ct_elt.size(); i++){
				poly_scale_unoptimized(ct_elt[i], t, Bsk[i]);
			}
		}
#ifdef DEBUG
		for(const auto & part : ct_star_q){
			for(size_t i = 0; i < this->num_coeffs; i++){
				long degree = deg(part[i]);
				for(long d = 0; d <= degree; d++){
					assert(in_range(part[i][d], qi[i]));
				}
			}
		}
		for(const auto & part : ct_star_Bsk){
			for(size_t i = 0; i < Bsk.size(); i++){
				long degree = deg(part[i]);
				for(long d = 0; d <= degree; d++){
					assert(in_range(part[i][d], Bsk[i]));
				}
			}
		}
#endif		
		//Step 3: fast flooring, result returned is in Bsk
		//Step 4: SK conversion
		vector< vector<ZZX> > floor_sk(3);
		for(size_t i = 0; i < floor_sk.size(); i++){
			//TODO tracked error to here
			floor_sk[i] = fastRNSFloor(ct_star_q[i], ct_star_Bsk[i], optimized);
			//Clear memory here
			/*
			for(auto & x : ct_star_q[i]){
				x.kill();
			}
			for(auto & x : ct_star_Bsk[i]){
				x.kill();
			}
			*/
			floor_sk[i] = FastBconvSK(floor_sk[i]);
		}
		//Relinearization

		relin(floor_sk[0], floor_sk[1], floor_sk[2], optimized);

		overall_end = high_resolution_clock::now();

		return ctext(floor_sk[0], floor_sk[1]);

	}

	//Return 0 if parameters are correctly set
	static int opt_params(unsigned int & log_q, unsigned int & m_power, unsigned int & num_coeffs){
		if((!log_q) && (!m_power) && (!num_coeffs)){
			return 1;
		}
		if(log_q && m_power && num_coeffs){
			return (log_q > m_power << (num_coeffs-1));
		}

		if(!log_q){
			if((!m_power) || (!num_coeffs)){
				return 1;
			}
			log_q = m_power << (num_coeffs-1);
			return 0;
		}

		if(!m_power){
			if((!log_q) || (!num_coeffs)){
				return 1;
			}
			m_power = log_q >> (num_coeffs-1);
			return 0;
		}

		if(!num_coeffs){
			if((!log_q) || (!m_power)){
				return 1;
			}
			num_coeffs = 1 + ceil(log2(log_q)) - ceil(log2(m_power));
			return 0;
		}

		return 0;
	}


};

//Old code

/*

inline long max_poly_deg(const vector<ZZX> & a){
	long deg = 0;
	for(const auto & x : a){
		deg = max(deg, degree(a));
	}
	return deg;
}

//Assumes qi/q, qi, and q/qi are precomputed
//T should be one of vector<ZZ> or ZZX
vector<ZZ> FastBConv(vector<ZZ> & x, const vector<ZZ> & qi, 
	const vector<ZZ> & qi_q, const vector<ZZ> & q_qi,
	 const vector<ZZ> & bases){
	//Implementing this by computing one large sum, then one final sum
#ifdef DEBUG
	//What to do if x is a ZZX?
	assert(x.size() == qi_q.size());
	assert(q_factors.size() == q_qi.size());
	//Also should check product of q_factors is q
#endif	
	ZZ sum(0);
	for(size_t i = 0; i < qi.size()){
		sum += ((x[i]*qi_q[i])%qi[i])*q_qi[i];
	}
	vector<ZZ> ret;
	ret.reserve(bases.size());
	for(size_t j = 0; j < bases.size(); j++){
		ret.push_back(sum % bases[j]);
	}
	return ret;
}

//Assumes qi/q, qi, and q/qi are precomputed, and that x is in RNS form
//Pass an optional multicand
vector<ZZX> FastBConv(vector<ZZX> & x, const vector<ZZ> & qi, 
	const vector<ZZ> & qi_q, const vector<ZZ> & q_qi,
	 const vector<ZZ> & bases, const ZZ & mcand = 1){
#ifdef DEBUG
	//What to do if x is a ZZX?
	assert(x.size() == qi_q.size());
	assert(q_factors.size() == q_qi.size());
	//Also should check product of q_factors is q
#endif	
	bool scale = (mcand == 1);
	long max_deg = max_poly_deg(x);
	ZZ sum;
	vector<ZZX> result(bases.size());
	//Do the FastBConv for each coefficient
	for(long degree_idx = 0; degree_idx < max_deg; degree_idx++){
		sum = 0;
		//Find sum over all matching coefficients of x
		//coeff returns 0 if out of range
		for(size_t i = 0; i < x.size(); i++){
			if(scale){
				sum += ((coeff(x[i], degree_idx)*qi_q[i])%qi[i])*q_qi[i];
			}
			else{
				sum += ((coeff(x[i], degree_idx)*qi_q[i]*mcand)%qi[i])*q_qi[i];
			}
		}
		//Fill in bases
		for(size_t j = 0; j < bases.size(); j++){
			SetCoeff(result[j], degree_idx, sum % bases[j]);
		}
	}
	return result;
}

//Assumes qi/q, qi, and q/qi are precomputed, and that x is in RNS form
ZZX FastBConv(vector<ZZX> & x, const vector<ZZ> & qi, 
	const vector<ZZ> & qi_q, const vector<ZZ> & q_qi,
	 const ZZ & base, bool negate = false){
#ifdef DEBUG
	//What to do if x is a ZZX?
	assert(x.size() == qi_q.size());
	assert(q_factors.size() == q_qi.size());
	//Also should check product of q_factors is q
#endif	
	long max_deg = max_poly_deg(x);
	ZZ sum;
	ZZX result;
	//Do the FastBConv for each coefficient
	for(long degree_idx = 0; degree_idx < max_deg; degree_idx++){
		sum = 0;
		//Find sum over all matching coefficients of x
		//coeff returns 0 if out of range
		for(size_t i = 0; i < x.size(); i++){
			sum += ((coeff(x[i], degree_idx)*qi_q[i])%qi[i])*q_qi[i];
		}
		//Fill in bases
		if(!negate){
			SetCoeff(result, degree_idx, sum % bases[j]);
		}
		else{
			SetCoeff(result, degree_idx, -(sum % bases[j]));
		}
		
	}
	return result;
}



//bases[0] == t
//bases[1] == gamma
//WARNING may modify the contents of ct (specifically scaling by t*gamma)
//TODO rewrite - account for 2-component ciphertext - take in ct0, ct1 (in RNS form)
ZZX Bajard_decrypt(const vector<ZZX> & ct0, const vector<ZZX> & ct1, const vector<ZZX> & s, 
	const vector<ZZ> & bases, const ZZ & base_prod, const ZZ & q, const vector<ZZ> & qi, 
	const vector<ZZ> & qi_q, const vector<ZZ> & q_qi, 
	const vector<ZZ> & q_inv_mod_bases, const ZZ & gamma_inv_mod_t){
	const static unsigned int t_idx = 0;
	const static unsigned int gamma_idx = 1;
	//Efficiency - declare ct non-const, and make it the caller's responsibility to save the ciphertext?
	//vector<ZZX> ct_multiplied = ct;
#ifdef DEBUG
	assert(ct0.size() == ct1.size());
	assert(base_prod == bases[t_idx]*bases[gamma_idx]);
#endif	

	//Evaluate ciphertext on secret key
	vector<ZZX> ct(ct0.size());
	//Do multiplication RNS componentwise
	for(size_t i = 0; i < ct.size(); i++){
		//ct[i] = ct0[i] + (ct1[i]*s[i]);
		ct[i] = poly_ring_add(ct0[i], poly_ring_mult(ct1[i]*s[i]));
	}
	//Scale by gamma*t and compute mod
	for(auto & val : ct){
		val *= base_prod;
		cmod_inplace(val, q);
	}
	//Line 2 of Bajard Alg. 1
	ZZX s_gamma = FastBConv(ct, qu, qi_q, q_qi, bases[gamma_idx], true)*q_inv_mod_bases[gamma_idx];
	ZZX s_t = FastBConv(ct, qu, qi_q, q_qi, bases[t_idx], true)*q_inv_mod_bases[t_idx];
	long s_gamma_deg = degree(s_gamma);
	long s_t_deg = degree(s_t);
	//TODO modify these to not 
	for(long s_gamma_idx = 0; s_gamma_idx < s_gamma_deg; s_gamma_idx++){
		s_gamma[s_gamma_idx] %= bases[gamma_idx];
	}
	for(long s_t_idx = 0; s_t_idx < s_t_deg; s_t_idx++){
		s_t[s_t_idx] %= bases[t_idx];
	}
	//Lines 4 & 5
	ZZX m_t;
	for(long m_t_idx = 0; m_t_idx < s_gamma_deg; m_t_idx++){
		SetCoeff(m_t, m_t_idx, 
cmod((coeff(s_t, m_t_idx) - cmod(coeff(s_gamma, m_t_idx), bases[gamma_idx]))*gamma_inv_mod_t ), bases[t_idx]);
	}
	return m_t;
}

//Takes in c" \in Bsk U m~
//Returned vector is indexed in the same order as Bsk
//Assume c_double is indexed in order Bsk, then m~
vector<ZZ> SmMRq(const vector<ZZ> & c_double, const vector<ZZ> & Bsk, 
	const ZZ & m_tilde, const vector<ZZ> & m_tilde_inv_mod_m, 
	const ZZ & q, const ZZ & q_inv_mod_m_tilde){
#ifdef DEBUG
	assert(c_double.size() == Bsk.size());
#endif	
	ZZ q_r_m_tilde = q*cmod((-c_double[m_tilde_idx])*q_inv_mod_m_tilde, m_tilde);
	vector<ZZ> c_prime;
	c_prime.reserve(Bsk.size());
	//Make sure to skip the last bit of Bsk
	for(size_t i = 0; i < Bsk.size()-1; i++){
		//m == Bsk[i]
		//Line 3 of algorithm 2
		c_prime.push_back(((c_double[i]+q_r_m_tilde)*m_tilde_inv_mod_m[i])%Bsk[i]);
	}
	return c_prime;
}

vector<ZZX> SmRq(const vector<ZZX> c_double, const vector<ZZ> & Bsk, 
	const ZZ & m_tilde, const vector<ZZ> & m_tilde_inv_mod_m, 
	const ZZ & q, const ZZ & q_inv_mod_m_tilde){
#ifdef DEBUG
	assert(c_double.size() == Bsk.size());
#endif
	ZZ q_r_m_tilde = q*cmod((-c_double[m_tilde_idx])*q_inv_mod_m_tilde, m_tilde);
	vector<ZZX> c_prime(Bsk.size());
	long max_deg = max_poly_deg(c_double);
	for(long deg_idx = 0; deg_idx < max_deg; deg_idx++){
		for(size_t base_idx = 0; base_idx < Bsk.size()-1; base_idx++){
			SetCoeff(c_prime[base_idx], deg_idx, ((coeff(c_double[base_idx])+q_r_m_tilde)*m_tilde_inv_mod_m)%Bsk[base_idx]);
		}
	}
	return c_prime;
}

vector<ZZX> centered_to_regular(const vector<ZZX> & a, const vector<ZZ> & bases){
#ifdef DEBUG
	assert(a.size() == bases.size());
#endif	
	vector<ZZX> ret = a;
	max_deg = max_poly_deg(ret);
	for(long deg_idx = 0; deg_idx < max_deg; deg_idx++){
		for(size_t base_idx = 0; base_idx < bases.size(); base_idx++){
			if(ret[base_idx][deg_idx] < 0){
				ret[base_idx][deg_idx] += bases[base_idx];
			}
		}
	}
}

void centered_to_regular_inplace(const vector<ZZX> & a, const vector<ZZ> & bases){
	#ifdef DEBUG
	assert(a.size() == bases.size());
#endif	
	max_deg = max_poly_deg(ret);
	for(long deg_idx = 0; deg_idx < max_deg; deg_idx++){
		for(size_t base_idx = 0; base_idx < bases.size(); base_idx++){
			if(a[base_idx][deg_idx] < 0){
				a[base_idx][deg_idx] += bases[base_idx];
			}
		}
	}
}

//fastRNSFloor from 
//Assumes a is centered mod q
vector<ZZX> fastRNSFloor(const vector<ZZX> a, 
	const ZZ & b, const ZZ & q, const vector<ZZ> & q_i, const ZZ & q_inv_mod_b, 
	const vector<ZZ> & qi_q, const vector<ZZ> & q_qi, const ZZ & t){
	vector<ZZX> a_mod_q = centered_to_regular(a, q);
	//Single-base reduction
	ZZX fastbconv_result = FastBConv(a_mod_q, q_i, qi_q, q_qi, b);
	//May not work if * operator not defined for ZZX and scalar - might have to handroll
	//ZZX intermed = (a-fastbconv_result)*q_inv_mod_b;
	ZZX intermed = (a-fastbconv_result);
	long deg = degree(intermed);
	for(long i = 0; i < deg; i++){
		intermed[i] *= q_inv_mod_b;
		intermed[i] %= b;
	}
	return intermed;
}

//fastRNSFloor from 
//Assumes a is centered mod q
vector<ZZX> fastRNSFloor(const vector<ZZX> a, 
	const vector<ZZ> & b, const ZZ & q, const vector<ZZ> & q_i, const vector<ZZ> & q_inv_mod_b, 
	const vector<ZZ> & qi_q, const vector<ZZ> & q_qi, const ZZ & t = 1){
	bool scale = (t == 1);
	vector<ZZX> a_mod_q = centered_to_regular(a, q);
	//Single-base reduction
	ZZX fastbconv_result = FastBConv(a_mod_q, q_i, qi_q, q_qi, b);
	//May not work if * operator not defined for ZZX and scalar - might have to handroll
	//ZZX intermed = (a-fastbconv_result)*q_inv_mod_b;
	ZZX intermed;
	if(!scale){
		intermed = (a-fastbconv_result);
	}
	else{
		intermed = ((a*t)-fastbconv_result);
	}
	long deg = degree(intermed);
	for(long i = 0; i < deg; i++){
		for(size_t i = 0; i < q_inv_mod_b.size(); i++){
			intermed[i] *= q_inv_mod_b;
			intermed[i] %= b;
		}
		
	}
	return intermed;
}
*/
