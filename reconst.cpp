#include <iostream>
#include <cassert>
#include <sstream>

#include <gmp.h>
#include <gmpxx.h>

#include "utilities.h"

#define DEBUG 1

#define EARLY_CMOD 0

//Debug settings
#if DEBUG
const unsigned int K_NUMBITS = 5;
const int NUMTRIALS = 100;
#else
//Timing settings
const unsigned int K_NUMBITS = 1000;
const unsigned int NUMTRIALS = 1000000;
#endif

const unsigned int CLOCKS_PER_MS = (CLOCKS_PER_SEC/1000);

#define TIME_OPERATIONS 1
//Macro to log timing
//string passed must have no spaces in the interior, and at least one trailing
#if TIME_OPERATIONS
#define LOG(op,st,dur,stream,type) { \
	st = clock(); \
	op; \
	dur = (clock() - start)/(double) CLOCKS_PER_MS; \
	stream << type << duration << std::endl; \
}
#else
#define LOG(op,st,dur,stream,type) { op; }
#endif

using namespace std;

int main(int argc, char ** argv){

	ostringstream os;
	bool test_primes = false;
	bool test_powers_two = false;
	bool test_powers_two_optimized = false;
	//Get mode
	if(argc != 2){
		cout << "Enter exactly one of p (primes), t (power of two), o (optimized power-of-two)" << endl;
		return 0;
	}
	switch(argv[1][0]){
		case 'p':{
			test_primes = true;
			break;
		}
		case 't':{
			test_powers_two = true;
			break;
		}
		case 'o':{
			test_powers_two_optimized = true;
			break;
		}
	}	
	if(!(test_primes || test_powers_two || test_powers_two_optimized)){
		cout << "Must give at least one option!" << endl;
		return 0;
	}

	//Force GMP to have enough precision with larger numbers of bits
	if(mpf_get_default_prec() < K_NUMBITS){
		mpf_set_default_prec(K_NUMBITS);
	}

	//Testing regular reconstruct_primes with actual primes
	//WARNING: sizes here are hardcoded
	vector<mpz_class> coprimes(2);
	mpz_class one = 1;
	if(test_primes){
		mpz_class lower_bound = (one << K_NUMBITS) - 1;
		mpz_nextprime(coprimes[0].get_mpz_t(), lower_bound.get_mpz_t());
		mpz_nextprime(coprimes[1].get_mpz_t(), coprimes[0].get_mpz_t());
	}
	else {
		coprimes[0] = (one << K_NUMBITS) + 1;
		coprimes[1] = (one << K_NUMBITS) - 1;
	}


	//Precompute product and inverses
	mpz_class coprimes_product = product(coprimes);
	vector<mpz_class> regular_mcands = CRT_mcands(coprimes);
	vector<mpz_class> inv_mod_mcands = inverses_reduced(regular_mcands, coprimes);
	//Not yet reduced
	vector<mpz_class> partial_products = coeff_multiply(regular_mcands, inv_mod_mcands);

	
	//Print primes - how large are they?
	//cout << "Primes: " << prime_plus << endl << prime_minus << endl;

	
	//Get x and y to test - assume for now that these are modular products
	gmp_randstate_t state;
	gmp_randinit_default(state);
	mpz_class x, y, a, b;
	for(unsigned int i = 0; i < NUMTRIALS; i++){
		mpz_urandomb(a.get_mpz_t(), state, K_NUMBITS);
		mpz_urandomb(b.get_mpz_t(), state, K_NUMBITS);


		mpz_class naive_product = a*b;
		naive_product = centered_mod(naive_product, coprimes_product);
		/*
		mpz_urandomb(x.get_mpz_t(), state, K_NUMBITS);
		mpz_urandomb(y.get_mpz_t(), state, K_NUMBITS);
		*/
		mpz_class a0 = centered_mod(a, coprimes[0]);
		mpz_class a1 = centered_mod(a, coprimes[1]);
		mpz_class b0 = centered_mod(b, coprimes[0]);
		mpz_class b1 = centered_mod(b, coprimes[1]);


		//First do version already in Overleaf, then see if we can get away without the modulus
		x = a0*b0;
		y = a1*b1;


		mpz_class result = 0;
		//Allocate enough room for result to avoid reallocation later
		
		mpz_realloc2(result.get_mpz_t(), 4*K_NUMBITS);
		/*
		mpz_realloc2(x.get_mpz_t(), 2*K_NUMBITS);
		mpz_realloc2(y.get_mpz_t(), 2*K_NUMBITS);
		*/

		if(test_powers_two_optimized){
			//mpz_class x_prime, y_prime, result;
			result = 0;
			double duration;
			double start; 
			double total_start = clock();
			//Try C functions instead of C++ overloaded bitshift
			//Actually may not need these

			//Version with extra mods
#if EARLY_CMOD			

			LOG(mpz_mul_2exp(x.get_mpz_t(), x.get_mpz_t(), (K_NUMBITS-1)),start,duration,os,"bitshift ")
			LOG(mpz_mul_2exp(y.get_mpz_t(), y.get_mpz_t(), (K_NUMBITS-1)),start,duration,os,"bitshift ")
			LOG(x = centered_mod(x, coprimes[0]),start,duration,os,"cmod_regular ")
			LOG(y = centered_mod(y, coprimes[1]),start,duration,os,"cmod_regular ")
			LOG(result = x+y,start,duration,os,"add/sub ")
			LOG(mpz_mul_2exp(result.get_mpz_t(), result.get_mpz_t(), K_NUMBITS),start,duration,os,"bitshift ")
			LOG(result += y,start,duration,os,"add/sub ")
			LOG(result -= x,start,duration,os,"add/sub ")
			//Trying simple/regular mod for the last one
			/*
			size_t result_size = mpz_sizeinbase(result.get_mpz_t(), 2);
			std::cerr << "Result size (bits): " << result_size << '\n';
			*/
			LOG(result = cmod_simple(result, coprimes_product),start,duration,os,"cmod_simple ")
			
      		
			/*
			start = clock();
			mpz_mul_2exp(x.get_mpz_t(), x.get_mpz_t(), (K_NUMBITS-1));
			duration = (clock() - start)/(double) CLOCKS_PER_MS;
			os << "bitshift " << duration << endl;
			
			start = clock();
			mpz_mul_2exp(y.get_mpz_t(), y.get_mpz_t(), (K_NUMBITS-1));
			duration = (clock() - start)/(double) CLOCKS_PER_MS;
			os << "bitshift " << duration << endl;

			start = clock();
			x = centered_mod(x, coprimes[0]);
			duration = (clock() - start)/(double) CLOCKS_PER_MS;
			os << "cmod_regular " << duration << endl;		

			start = clock();
			y = centered_mod(y, coprimes[1]);
			duration = (clock() - start)/(double) CLOCKS_PER_MS;
			os << "cmod_regular " << duration << endl;	

			start = clock();
			result = x+y;
			duration = (clock() - start)/(double) CLOCKS_PER_MS;
			os << "add/sub " << duration << endl;

			start = clock();
			mpz_mul_2exp(result.get_mpz_t(), result.get_mpz_t(), K_NUMBITS);
			duration = (clock() - start)/(double) CLOCKS_PER_MS;
			os << "bitshift " << duration << endl;

			start = clock();
			result -= x;
			duration = (clock() - start)/(double) CLOCKS_PER_MS;
			os << "add/sub " << duration << endl;

			start = clock();
			result += y;
			duration = (clock() - start)/(double) CLOCKS_PER_MS;
			os << "add/sub " << duration << endl;

			start = clock();
			result = cmod_simple(result, coprimes_product);
			duration = (clock() - start)/(double) CLOCKS_PER_MS;
			os << "cmod_simple " << duration << endl;

			*/


			/*
			x = x << (K_NUMBITS-1);
			y = y << (K_NUMBITS-1);		
			*/
#else			

			LOG(result = x+y,start,duration,os,"add/sub ")
			LOG(mpz_mul_2exp(result.get_mpz_t(), result.get_mpz_t(), K_NUMBITS),start,duration,os,"bitshift ")
			LOG(result += y,start,duration,os,"add/sub ")
			LOG(result -= x,start,duration,os,"add/sub ")
			LOG(mpz_mul_2exp(result.get_mpz_t(), result.get_mpz_t(), K_NUMBITS-1),start,duration,os,"bitshift ")
			//Trying simple/regular mod for the last one
			/*
			size_t result_size = mpz_sizeinbase(result.get_mpz_t(), 2);
			std::cerr << "Result size (bits): " << result_size << '\n';
			*/
			LOG(result = centered_mod(result, coprimes_product),start,duration,os,"cmod_regular ")

			//Optimized version, without earlier moduli
			/*
			result = x+y;
			duration = (clock() - start)/(double) CLOCKS_PER_MS;
			os << "add/sub " << duration << endl;
			
			start = clock();
			mpz_mul_2exp(result.get_mpz_t(), result.get_mpz_t(), K_NUMBITS);
			duration = (clock() - start)/(double) CLOCKS_PER_MS;
			os << "bitshift " << duration << endl;

			start = clock();
			result += y;
			duration = (clock() - start)/(double) CLOCKS_PER_MS;
			os << "add/sub " << duration << endl;

			start = clock();
			result -= x;
			duration = (clock() - start)/(double) CLOCKS_PER_MS;
			os << "add/sub " << duration << endl;

			//size_t result_size = mpz_sizeinbase(result.get_mpz_t(), 2);
			
			start = clock();
			mpz_mul_2exp(result.get_mpz_t(), result.get_mpz_t(), K_NUMBITS-1);
			duration = (clock() - start)/(double) CLOCKS_PER_MS;
			os << "bitshift " << duration << endl;

			start = clock();
			result = centered_mod(result, coprimes_product);			
			duration = (clock() - start)/(double) CLOCKS_PER_MS;
      		os << "cmod_regular " << duration << endl;      		
      		//std:cerr << "Result size (bits): " << result_size << '\n';
      		*/
      		
#endif

			double total_duration = (clock() - total_start)/(double) CLOCKS_PER_MS;
			os << total_duration << endl;

      		if(result != naive_product){
      			std::cerr << "Nonequal: result " << result << " actual " << naive_product << endl;
      			assert(result == naive_product);
      		}
      		
		}
		else{
			vector<mpz_class> inputs(2);
			inputs[0] = x;
			inputs[1] = y;
			result = 0;
			mpz_class tmp_MAC = 0;
			double duration;
			double start;
			double total_start = clock();
			//mpz_class result = reconstruct_primes(x, y, prime_plus, prime_minus, primes_product);
			for(size_t j = 0; j < coprimes.size(); j++){

				LOG(tmp_MAC = inputs[j] * partial_products[j],start,duration,os,"multiply ")
				LOG(result += tmp_MAC,start,duration,os,"add/sub ")

				/*
				start = clock();
				tmp_MAC = inputs[j] * partial_products[j];
				duration = (clock() - start)/(double) CLOCKS_PER_MS;
				os << "multiply " << duration << endl;

				start = clock();
				result += tmp_MAC;
				duration = (clock() - start)/(double) CLOCKS_PER_MS;
				os << "add/sub " << duration << endl;
				*/
			}

			/*
			size_t result_size = mpz_sizeinbase(result.get_mpz_t(), 2);
			std::cerr << "Result size (bits): " << result_size << '\n';
			*/
			LOG(result = centered_mod(result, coprimes_product),start,duration,os,"cmod_regular ")

			double total_duration = (clock() - total_start)/(double) CLOCKS_PER_MS;
			os << total_duration << endl;

			/*
			start = clock();
			result = centered_mod(result, coprimes_product);
			duration = (clock() - start)/(double) CLOCKS_PER_MS;
			os << "cmod_regular " << duration << endl;
			*/

      		if(result != naive_product){
      			std::cerr << "Nonequal: result " << result << " actual " << naive_product << endl;
      			assert(result == naive_product);
      		}
		}
	}

	cout << os.str();
	


	

	return 0;
}