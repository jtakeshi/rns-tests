#include <iostream>
#include <sstream>

#include <gmp.h>
#include <gmpxx.h>

#include "utilities.h"

#define DEBUG 0

#if DEBUG
const unsigned int NUMTRIALS = 10;
#else
const unsigned int NUMTRIALS = 10000000;
#endif

const unsigned int K_MINBITS = 64;
const unsigned int K_MAXBITS = 8192;


#define CLOCKS_PER_MS (CLOCKS_PER_SEC/1000)

using namespace std;

int main(int argc, char ** argv){
	/*
	if(argc != 3){
		cout << "Usage: ./cmod numtrials mode" << endl;
		cout << "mode should be 0 if the integer-only centered mod should be tested, else we will use the floating DWR" << endl;
		cout << "numtrials can be 0 to invoke the default " << endl;
		return 0;
	}
	*/
	/*
	unsigned int numtrials = atoi(argv[1]);
	if(!numtrials){
		numtrials = NUMTRIALS;
	}
	*/
	//bool DWR = atoi(argv[2]) != 0;
	assert(K_MINBITS < K_MAXBITS);
	ostringstream os;
	

	for(unsigned int bitsize = K_MINBITS; bitsize <= K_MAXBITS; bitsize*=2){
		gmp_randstate_t state;
		gmp_randinit_default(state);
		//Force mpf_t precision
		if(mpf_get_default_prec() < bitsize){
			mpf_set_default_prec(bitsize);
		}
		for(unsigned int i = 0; i < NUMTRIALS; i++){
				mpz_class a, b, result;
				mpz_urandomb(a.get_mpz_t(), state, bitsize);
				mpz_urandomb(b.get_mpz_t(), state, bitsize);
				double start = clock();
				//Testing with mpf-based cmod
				result = centered_mod_DWR(a, b);
				double duration = (clock() - start)/(double) CLOCKS_PER_MS;
				os << "bits_" << bitsize << ' ' << duration << endl;
		}

	}
	cout << os.str() << endl;
	return 0;
}