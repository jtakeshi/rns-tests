#include <cassert>

#include <NTL/ZZ.h>
#include <NTL/ZZX.h>
//Some thread boosting here
#include <NTL/BasicThreadPool.h>

using namespace NTL;

//Calculate x*(2^y)-1 mod 2^g-1
ZZ shift_mod_min1(const ZZ & x, const unsigned int y, const unsigned int g){
#ifdef DEBUG
	assert(y < g);
	ZZ g_check(1);
	g_check <<= g;
	g_check -= 1;
	assert(x < g_check);
#endif	
	ZZ high_bits;
	//high_bits = x >> (g-y);
	RightShift(high_bits, x, g-y);
	ZZ low_bits;
	LeftShift(low_bits, x, y);
	//low_bits = x << y;
	//low_bits &= g_mask;
	trunc(low_bits, low_bits, g-1);
	return high_bits | low_bits; //
}


ZZ shift_mod_plus1(const ZZ & x, const unsigned int y, const unsigned int g){
#ifdef DEBUG
	assert(y < g);
	ZZ g_check(1);
	g_check <<= g;
	g_check += 1;
	assert(x < g_check);
#endif	
	ZZ high_bits;
	//high_bits = x >> (g-y);
	RightShift(high_bits, x, g-y);
	ZZ low_bits;
	LeftShift(low_bits, x, y);
	//low_bits = x << y;
	//low_bits &= g_mask;
	trunc(low_bits, low_bits, g-1);
	if(low_bits >= high_bits){
		low_bits -= high_bits;
		return low_bits;
	}
	else{
		//TODO optimize this
		high_bits = (ZZ(1) << g) - high_bits;
		high_bits += 2;
		high_bits -= low_bits;
		return high_bits;
	}
}

ZZ mod_min_one(const ZZ & x_in, const unsigned int g, const ZZ & g_modulus){
#ifdef DEBUG
	ZZ g_check(1);
	g_check <<= g;
	g_check -= 1;
	if(g_modulus != g_check){
		cerr << "ERROR: modulus " << g_modulus << " check " << g_check << " g " << g << endl;
	}
	assert(g_modulus == g_check);
	assert(x_in >= 0);
	if(x_in >= g_modulus*g_modulus){
		cerr << "ERROR: modulus " << g_modulus << " check " << g_check << " g " << g << " arg " << x_in << endl;
	}
	assert(x_in < g_modulus*g_modulus);
#endif	
	ZZ y;
	//y = x_in >> g;
	RightShift(y, x_in, g);
	ZZ x;
	trunc(x, x_in, g-1); // = x_in & g_mask;
	x += y;
	if(x >= g_modulus){
		x -= g_modulus;
	}
#ifdef DEBUG
	assert(x >= 0);
	assert(x < g_modulus);
#endif	
	return x;
}

ZZ mod_plus_one(const ZZ & x_in, const unsigned int g, const ZZ & g_modulus){
#ifdef DEBUG
	ZZ g_check(1);
	g_check <<= g;
	g_check += 1;
	if(g_modulus != g_check){
		cerr << "ERROR: modulus" << g_modulus << " check " << g_check << " g " << g << endl;
	}
	assert(g_modulus == g_check);
	assert(x_in >= 0);
	assert(x_in < g_modulus*g_modulus);
	//g_check.kill();
#endif	
	ZZ y;
	RightShift(y, x_in, g);
	ZZ x;
	trunc(x, x_in, g-1); // = x_in & g_mask;
	if(x >= y){
		x -= y;
	}
	else{
		x += g_modulus;
		x -= y;
	}
#ifdef DEBUG
	assert(x >= 0);
	assert(x < g_modulus);
#endif	
	return x;
}

ZZX poly_add_simplemod(const ZZX & a, const ZZX & b, const ZZ & modulus){
	ZZX ret;
	long degree = max(deg(a), deg(b));
	ret.SetLength(degree+1);
	ZZ tmp;
	for(long i = 0; i <= degree; i++){
		add(tmp, coeff(a, i), coeff(b, i));
		SetCoeff(ret, i, tmp);
		//ret[i] = a[i];
		//ret[i] += b[i];
		if(ret[i] >= modulus){
			ret[i] -= modulus;
		}
	}
	return ret;
}

void poly_add_inplace(ZZX & a, const ZZX & b, const ZZ & modulus){
#ifdef DEBUG
	assert(modulus > 0);
#endif	
	long len = max(deg(a), deg(b));
	//a.SetLength(len+1);
	ZZ tmp;
	for(long i = 0; i <= len; i++){
#ifdef DEBUG
		assert(coeff(a, i) >= 0);
		assert(coeff(b, i) >= 0);

		if(coeff(a, i) >= modulus){
			cerr << "ERROR poly_add_inplace: a[" << i << "] " << coeff(a, i) 
				 << " modulus " << modulus << endl;
		}
		if(coeff(b, i) >= modulus){
			cerr << "ERROR poly_add_inplace: b[" << i << "] " << coeff(b, i) 
				 << " modulus " << modulus << endl;
		}

		assert(coeff(a, i) < modulus);
		assert(coeff(b, i) < modulus);
#endif		
		add(tmp, coeff(a, i), coeff(b, i));
		SetCoeff(a, i, tmp);
		//a[i] += coeff(b, i);
		if(a[i] >= modulus){
			a[i] -= modulus;
		}
#ifdef DEBUG
		assert(a[i] >= 0);
		assert(a[i] < modulus);
#endif		
	}
	return;
}

//Use me in SmMRq!
ZZX poly_mult_add(const ZZX & c, const ZZX & r, const ZZ & q, const ZZ & modulus){
	ZZX ret(0);
	//ret = c;
	long len = max(deg(c), deg(r));
	long r_deg = deg(r);
	SetCoeff(ret, len, 0);
	ZZ tmp;
	for(long i = 0; i <= len; i++){
		if(i > r_deg){
			SetCoeff(ret, i, coeff(c, i));
			//ret[i] = c[i];
		}
		else{
			mul(tmp, q, coeff(r, i));
			//tmp = q*r[i];
			tmp %= modulus;
			tmp += coeff(c, i);
			SetCoeff(ret, i, tmp);
			//add(ret[i], tmp, c[i]);
		}
		//ret[i] += tmp;
		if(coeff(ret, i) >= modulus){
			ret[i] -= modulus;
		}
		/*
		ret[i] *= q;
		ret[i] %= modulus;
		*/
	}
	return ret;
}

ZZX poly_sub_simplemod(const ZZX & a, const ZZX & b, const ZZ & modulus){
	ZZX ret;
	long degree = max(deg(a), deg(b));
	ret.SetLength(degree+1);
	ZZ tmp_a, tmp_b;
	for(long i = 0; i <= degree; i++){
		tmp_a = coeff(a, i);
		tmp_b = coeff(b, i);
		if(tmp_a > tmp_b){
			sub(ret[i], tmp_a, tmp_b);
			/*
			ret[i] = a[i];
			ret[i] -= b[i];
			*/
		}
		else{
			sub(ret[i], modulus, b[i]);
			ret[i] += a[i];
			/*
			ret[i] = modulus;
			ret[i] -= b[i];
			ret[i] += a[i];
			*/
		}
	}
	return ret;
}

ZZX poly_mult(const ZZX & a, const ZZX & b, const ZZ & modulus, const long poly_mod_deg){
	ZZX ret = a*b;
	long degree = deg(ret);
	for(long i = degree; i >= 0; i--){
		
		if(i >= poly_mod_deg){
			long idx = i % poly_mod_deg;
			//ret[i] %= modulus;
			ret[idx] -= ret[i];
		}
		else{
			int s = sign(ret[i]);
			if(!s){
				continue;
			}
			//Flip into positive range
			if(s == -1){
				NTL::negate(ret[i], ret[i]);
				//sub(ret[i], modulus, ret[i]);
				//ret[i] = modulus - ret[i];
			}
			ret[i] %= modulus;
			if(s == -1 && !IsZero(ret[i])){
				//Undo the flip
				sub(ret[i], modulus, ret[i]);
			}
#ifdef DEBUG		
			assert(ret[i] >= 0);
			assert(ret[i] < modulus);
#endif		
		}

	}
	trunc(ret, ret, poly_mod_deg-1);
	return ret;
}

ZZX poly_inner_optimized(const ZZX & a, const ZZX & b, const ZZ & modulus, bool fermat, unsigned int g){
	ZZX ret;
#ifdef DEBUG
	ZZ check;
	set(check);
	check <<= g;
	if(fermat){
		check++;
	}
	else{
		check--;
	}
	assert(check == modulus);
#endif	
	long degree = min(deg(b), deg(a));
	ret.SetLength(degree+1);
	ZZ tmp;
	for(long i = 0; i <= degree; i++){
#ifdef DEBUG
		assert(a[i] < modulus);
		assert(b[i] < modulus);
		assert(a[i] >= 0);
		assert(b[i] >= 0);
#endif		
		mul(tmp, a[i], b[i]);
		if(!fermat){
			SetCoeff(ret, i, mod_min_one(tmp, g, modulus));
			//ret[i] = mod_min_one(ret[i], g, modulus);
		}else{
			SetCoeff(ret, i, mod_plus_one(tmp, g, modulus));
			//ret[i] = mod_plus_one(ret[i], g, modulus);
		}
#ifdef DEBUG
		assert(ret[i] < modulus);
		assert(ret[i] >= 0);
#endif				
	}
	return ret;
}

//Last argument is g
//TODO take in modulus?
ZZX poly_mult_optimized(const ZZX & a, const ZZX & b, bool fermat, unsigned int g, const long poly_mod_deg){
#ifdef DEBUG
	assert(deg(a) == deg(b));
#endif	
	ZZX ret = a*b;
	long degree = deg(ret);
	ZZ modulus;
	set(modulus);
	modulus <<= g;
	if(fermat){
		modulus++;
	}
	else{
		modulus--;
	}
	assert(degree < 2*poly_mod_deg); 
#ifdef DEBUG
	ZZ bound = poly_mod_deg*modulus*modulus;
#endif	
	//TODO reverse loop direction
	for(long i = degree; i >= 0; i--){
#ifdef DEBUG
		assert(coeff(ret, i) < bound);
#endif		
		if(i >= poly_mod_deg){
			long idx = i % poly_mod_deg;
			SetCoeff(ret, idx, -ret[i]);			
		}
		else{
			//ret[i] %= modulus;
			int s = sign(ret[i]);
			if(!s){
				continue;
			}
			//Flip into positive range
			if(s == -1){
				SetCoeff(ret, i, -ret[i]);
				//NTL::negate(ret[i], ret[i]);
				//sub(ret[i], modulus, ret[i]);
				//ret[i] = modulus - ret[i];
			}
			if(!fermat){
				ret[i] = mod_min_one(coeff(ret, i), g, modulus);
			}else{
				ret[i] = mod_plus_one(coeff(ret, i), g, modulus);
			}
#ifdef DEBUG		
			if((ret[i] >= 0) || (ret[i] < modulus)){
				cerr << "ERROR: modulus failed: ret " << ret[i] << " g " << g << " modulus " << modulus << endl;
			}
			assert(ret[i] >= 0);
			assert(ret[i] < modulus);
#endif				
			if(s == -1 && !IsZero(ret[i])){
				//Undo the flip
				sub(ret[i], modulus, ret[i]);
			}
#ifdef DEBUG		
			assert(ret[i] >= 0);
			assert(ret[i] < modulus);
#endif	
		}
	
	}



	/*
	for(long i = degree; i >= 0; i--){
		if(i >= poly_mod_deg){
			long idx = i % (poly_mod_deg);
			//TODO analyze for sign
			int s = sign(coeff(ret, i));
			if(!s){
				continue;
			}
			if(s == -1){
				//Flip
				NTL::negate(ret[i], coeff(ret, i));
				//ret[i] = modulus - ret[i];
			}
#ifdef DEBUG
			assert(coeff(ret, i) >= 0);
#endif		
			if(!fermat){
				ret[i] = mod_min_one(coeff(ret, i), g, modulus);
			}else{
				ret[i] = mod_plus_one(coeff(ret, i), g, modulus);
			}
#ifdef DEBUG
			assert(ret[i] >= 0);
			assert(ret[i] < modulus);
#endif		
			if(s == -1 && !IsZero(ret[i])){
				//Flip back
				sub(ret[i], modulus, ret[i]);
			}

			//Negate and add

			//Handle the destination modulus first
			s = sign(ret[idx]);
			if(ret[idx] < 0){
				ret[idx] = modulus - ret[idx];
			}
			if(!fermat){
				ret[idx] = mod_min_one(ret[idx], g, modulus);
			}else{
				ret[idx] = mod_plus_one(ret[idx], g, modulus);
			}
			//Now do the actual (polynomial) modular reduction
			
			ret[idx] += ret[i];
			if(ret[idx] >= modulus){
				ret[idx] -= modulus;
			}

			clear(ret[i]);
		}
		else{
			if()
		}
		

#ifdef DEBUG
		assert(ret[idx] >= 0);
		assert(ret[idx] < modulus);
#endif		
	}
	*/

	trunc(ret, ret, poly_mod_deg-1);
	return ret;
}

ZZX poly_negate(const ZZX & x, const ZZ & modulus){
	ZZX ret;
	long len = deg(x);
	if(len < 0){
		return ret;
	}
	ret.SetLength(len+1);
	ZZ tmp;
	NTL_EXEC_RANGE(len+1, first, last)
	for(long i = first; i < last; i++){
		if(x[i] != 0){
			sub(tmp, modulus, x[i]);
		}
		else{
			clear(tmp);
		}
		SetCoeff(ret, i, tmp);
		
		//ret[i] = modulus - x[i];
	}
	NTL_EXEC_RANGE_END
	return ret;
}

void poly_scale_inplace(ZZX & x, const ZZ & y, const ZZ & modulus){
	long len = deg(x);
	for(long i = 0; i <= len; i++){
		x[i] *= y;
		x[i] %= modulus;
	}
	return;
}

void poly_scale_unoptimized(ZZX & x, const ZZ & y, const ZZ & modulus){
	long len = deg(x);
	ZZ tmp;
	for(long i = 0; i <= len; i++){
#ifdef DEBUG		
		assert(coeff(x, i) >= 0);
		assert(coeff(x, i) < modulus);
		assert(y >= 0);
		assert(y < modulus);
#endif				
		mul(tmp, coeff(x, i), y);
		SetCoeff(x, i, tmp);
		//ret[i] *= y;
		x[i] %= modulus;
#ifdef DEBUG		
		assert(x[i] >= 0);
		assert(x[i] < modulus);
#endif			
	}
	return;
}

void poly_scale_optimized(ZZX & x, const ZZ & y, bool fermat, const unsigned int g, const ZZ & modulus){
#ifdef DEBUG
	assert(y >= 0);
	assert(y < modulus);
#endif	
	long len = deg(x);
	ZZ tmp;
	for(long i = 0; i <= len; i++){
#ifdef DEBUG		
		assert(coeff(x, i) >= 0);
		assert(coeff(x, i) < modulus);
		assert(y >= 0);
		assert(y < modulus);
#endif		
		mul(tmp, x[i], y);
		if(!fermat){
			SetCoeff(x, i, mod_min_one(tmp, g, modulus));
		}else{
			SetCoeff(x, i, mod_plus_one(tmp, g, modulus));
		}
#ifdef DEBUG		
		assert(coeff(x, i) >= 0);
		assert(coeff(x, i) < modulus);
#endif		
	}
	return;
}

long bit_reverse(long v, const long N){
#ifdef DEBUG
  assert(v < N);
#endif  
  long r = v & 1; //Don't need the mask if upper_bit_bound is equal to word size
  for (long i = 2; i < N; i <<= 1){ //Loop goes for num. bits in representation, minus one
    r <<= 1;
    v >>= 1;
    r |= v & 1;
  }
  return r;
}

ZZX permute(const ZZX & x, const long N){
  ZZX ret;
  ret.SetLength(2*N);
  for(long i = 0; i < 2*N; i++){
    SetCoeff(ret, bit_reverse(i, 2*N), coeff(x, i));
  }
  return ret;
}

ZZX poly_inner_unoptimized(const ZZX & a, const ZZX & b, const ZZ & modulus){
  long degree = min(deg(a), deg(b));
  ZZX ret;
  ret.SetLength(degree+1);
  ZZ tmp;
  for(long d = 0; d <= degree; d++){
#ifdef DEBUG
	assert(a[d] < modulus);
	assert(b[d] < modulus);
	assert(a[d] >= 0);
	assert(b[d] >= 0);
#endif	
    mul(tmp, a[d], b[d]);
    SetCoeff(ret, d, tmp);
    ret[d] %= modulus;
#ifdef DEBUG
	assert(ret[d] < modulus);
	assert(ret[d] >= 0);
#endif	    
  }
  return ret;
}

ZZX NTT_unoptimized(const ZZX & x, const long N, const vector<ZZ> & w, const ZZ & modulus){
  ZZX a = permute(x, N);
  ZZ c, d;
  for(long M = 2; M <= 2*N; M *= 2){
    for(long j = 0; j <= (2*N)-1; j += M){
      long x_factor = (2*N)/M;
      for(long i = 0; i <= (M/2)-1; i++){
        long x = i*x_factor;
        long I = j+i;
        long J = I+(M/2);
        long t_idx = x & ((2*N)-1);
        
        c = a[I];
        mul(d, w[t_idx], a[J]);
        d %= modulus;
        
        add(a[I], d, c);
        if(a[I] >= modulus){
          a[I] -= modulus;
        }
        
        if(c >= d){
          sub(a[J], c, d);
        }
        else{
          sub(a[J], modulus, d);
          a[J] += c;
        }
      }
    }
  }
  //Now reduce
  for(long i = (2*N)-1; i >= N; i--){
    long idx = i % N;
    if(a[idx] >= a[i]){
      a[i] -= a[idx];
    }
    else{
      //Reuse c as tmp
      sub(c, modulus, a[i]);
      a[idx] += c;
    }
  }
#ifdef DEBUG
  for(long i = 0; i < N; i++){
    assert(a[i] < modulus);
    assert(a[i] >= 0);
  }
#endif  
  return trunc(a, N);
}

ZZX NTT_optimized(const ZZX & x, const long N, const vector<ZZ> & w, const ZZ & modulus, const unsigned int g, const bool fermat){
  ZZX a = permute(x, N);
  ZZ c, d;
  for(long M = 2; M <= 2*N; M *= 2){
    for(long j = 0; j <= (2*N)-1; j += M){
      long x_factor = (2*N)/M;
      for(long i = 0; i <= (M/2)-1; i++){
        long x = i*x_factor;
        long I = j+i;
        long J = I+(M/2);
        long t_idx = x & ((2*N)-1);
        
        c = a[I];
        mul(d, w[t_idx], a[J]);
        
        if(!fermat){
			    d = mod_min_one(d, g, modulus);
		    }else{
			    d = mod_plus_one(d, g, modulus);
		    }
        
        add(a[I], d, c);
        if(a[I] >= modulus){
          a[I] -= modulus;
        }
        
        if(c >= d){
          sub(a[J], c, d);
        }
        else{
          sub(a[J], modulus, d);
          a[J] += c;
        }
      }
    }
  }
  //Now reduce
  for(long i = (2*N)-1; i >= N; i--){
    long idx = i % N;
    if(a[idx] >= a[i]){
      a[idx] -= a[i];
    }
    else{
      a[idx] -= a[i];
      a[idx] += modulus;
      /*
      sub(c, modulus, a[i]);
      a[idx] += c;
      */
    }
  }
  trunc(a, a, N);
  assert(deg(a) < N);
#ifdef DEBUG
  for(long i = 0; i < N; i++){
    assert(coeff(a, i) < modulus);
    assert(coeff(a, i) >= 0);
  }
#endif  
  return a;
}

ZZX poly_mult_NTT_unoptimized(const ZZX & a, const ZZX & b, const long N, const vector<ZZ> & w, const vector<ZZ> & w_inverses, const ZZ & modulus){
  ZZX a_transformed = NTT_unoptimized(a, N, w, modulus);
  ZZX b_transformed = NTT_unoptimized(b, N, w, modulus);
  ZZX ret = poly_inner_unoptimized(a_transformed, b_transformed, modulus);
  return NTT_unoptimized(ret, N, w_inverses, modulus);
}

ZZX poly_mult_NTT_optimized(const ZZX & a, const ZZX & b, const long N, const vector<ZZ> & w, const vector<ZZ> & w_inverses, const ZZ & modulus, const unsigned int g, bool fermat){
  ZZX a_transformed = NTT_optimized(a, N, w, modulus, g, fermat);
  ZZX b_transformed = NTT_optimized(b, N, w, modulus, g, fermat);
  ZZX ret = poly_inner_optimized(a_transformed, b_transformed, modulus, fermat, g);
  return NTT_optimized(ret, N, w_inverses, modulus, g, fermat);
}




