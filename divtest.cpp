#include <iostream>
#include <random>
#include <gmp>
#include <gmpxx>

#define NUM_TRIALS 100000
#define CLOCKS_PER_MS (CLOCKS_PER_SEC/1000)

#define POWER_TWO 0
#define PRIME 1
#define PRODUCT_PRIMES 2

#define NUM_PRIMES 3

using namespace std;

int main(int argc, char ** argv){

  //Divuend can be a prime, a power of two, or a product of primes.
  
  if(argc < 2){
    cout << "Enter one of --two, --prime, --product" << endl;
  }
  unsigned int mode;
  if(!strcmp(argv[1], "--two")){
    mode = POWER_TWO
  }
  else if (!strcmp(argv[1], "--prime")){
    mode = PRIME;
  }
  else if (!strcmp(argv[1], "--product")){
    mode = PRODUCT_PRIMES;
  }
  else{
    cout << "Invalid option: " << argv[1] << endl;
    return 0;
  }

  random_device rd;
  //First, test divisions by powers of two
  unsigned int max_uint = -1;
  for(unsigned int i = 0; i < NUM_TRIALS; i++){
    unsigned int numerator;
    unsigned int denominator;
    if(mode == POWER_TWO){
      //Get a random power of two
      unsigned int exp = rd() % (sizeof(unsigned int)*8);
      unsigned int power = 1 << (exp-1);
      if(power == max_uint){
        power >> 1;
      }
      unsigned int rand_larger = rand() % (max_uint - power);
      while(rand_larger <= power){
        rand_larger += power;
      }
      numerator = rand_larger;
      denominator = power;
    }
    if(mode == PRIME){
    unsigned int prime = 0;
    mpz_class lower_mpz, prime_mpz;
      do{
        unsigned int lower_bound = rd();
        lower_mpz = lower_bound;
        mpz_nextprime(prime_mpz.get_mpz_t(), lower_mpz.get_mpz_t());
      } while(prime_mpz.fits_ulong_p());
      prime = prime_mpz.get_ui();
      denominator = prime;
      unsigned int rand_larger = rand() % (max_uint - prime);
      while(rand_larger <= prime){
        rand_larger += prime;
      }
      numerator = rand_larger;
      denominator = power;
    }
    //TODO finish up with product of primes
    
    
    
    double start = clock();
    unsigned int result = numerator / denominator;
    double duration = (clock() - start)/(double) CLOCKS_PER_MS;
    cout << duration << endl;
  }



}
