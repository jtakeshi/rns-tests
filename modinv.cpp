#include <iostream>
#include <string>
#include <sstream>

#include <gmp.h>
#include <gmpxx.h>

#include "utilities.h"

using namespace std;

//Returns magnitude bits set in sign-mag representation
std::vector<mp_bitcnt_t> bits_set(const mpz_class & arg){
  mpz_class unneg = (arg >= 0)? arg : -arg;
  std::vector<mp_bitcnt_t> result;
  mp_bitcnt_t num_set = mpz_popcount(unneg.get_mpz_t());
  result.reserve(num_set);
  mp_bitcnt_t last_set = 0;
  for(mp_bitcnt_t i = 0; i < num_set; i++){
    mp_bitcnt_t idx = mpz_scan1(unneg.get_mpz_t(), last_set);
    result.push_back(idx);
    last_set = idx+1;
/*
    if(!last_set){
      last_set++;    
    }
*/
  }
  return result;
}

int main(int argc, char ** argv){
	if(argc != 3){
		cout << "Usage: ./cmod k f" << endl;
		return 0;
	}

	unsigned int k = atoi(argv[1]);
	unsigned int f = atoi(argv[2]);
  mp_bitcnt_t power = 1;
  power <<= k;
  assert(f >= 1);
	//assert(mpz_divisible_2exp_p(mpz_class(power).get_mpz_t(), (1 << f)));
  //assert(power % mp_bitcnt_t(1 << f) == 0);
  assert(power >> f);
	
	vector<mpz_class> moduli;
  //mpz_class next_modulus;
	//Construct and print moduli
	for(unsigned int i = 0; i < f; i++){
		power /= 2;
    mpz_class next_modulus = 1;
    mpz_mul_2exp(next_modulus.get_mpz_t(), next_modulus.get_mpz_t(), power);
    //next_modulus <<= power;
    next_modulus++;
		moduli.push_back(next_modulus);
	}
  mpz_class final_modulus = 1;
  mpz_mul_2exp(final_modulus.get_mpz_t(), final_modulus.get_mpz_t(), power);
  final_modulus--;
	moduli.push_back(final_modulus);
	//Can also construct q here
	//Set output format
  //cout << ios::bin;
	mpz_class q = 1;

	cout << "Moduli for k=" << k << " and f=" << f << ':' << endl;
	for(const auto & x : moduli){
		q *= x;
		//cout << x << ' ';
    //Print mpz in hex
    //gmp_printf("%#Zx\n", x);
    //cout << ' ';
	}
	cout << '\n';
/*
  vector<mp_bitcnt_t> bits_q = bits_set(q);
	cout << "bits of q: ";
  for(const auto & x : bits_q){
    cout << x << ' ';
  }
	cout << '\n' << '\n';
*/

	mpz_class qi_q;
/*
  vector<mpz_class> qi_q_terms;
  vector<vector<mp_bitcnt_t> > bits;
*/
	for(size_t i = 0; i < moduli.size(); i++){
		//cout << "\tq_i: " << moduli[i] << endl;
		qi_q = q/moduli[i];
		//cout << "\tq/q_i: " << qi_q << endl;
		qi_q = mod_inv_regular(qi_q, moduli[i]);
		cout << "\tq_i / q mod q_i: ";
    //gmp_printf("%#Zx", qi_q);
    cout << ' ' << qi_q << endl;
    vector<mp_bitcnt_t> qi_q_bits = bits_set(qi_q);
    //qi_q_terms.push_back(qi_q);
    //bits.push_back(qi_q_bits);
    cout << "\tmcand bits set: ";
    for(const auto & x : qi_q_bits){
      cout << x << ' ';    
    }
    
    if(qi_q < 0){
      cout << "negative ";
    }
    cout << '\n';

    vector<mp_bitcnt_t> mcand_bits = bits_set(moduli[i]);
    cout << "\tmodulus bits set: ";
    for(const auto & x : mcand_bits){
      cout << x << ' ';
    }

    cout << '\n' << '\n';
    // << mpz_scan1(qi_q.get_mpz_t(), 1) << endl;

	}

/*
  for(size_t i = 0; i < qi_q_terms.size(); i++){
    for(size_t j = 0; j < qi_q_terms.size(); j++){
      if(i==j){continue;}
      if(bits[i] == bits[j]){
        if(qi_q_terms[i] != qi_q_terms[j]){
          cout << "Nonqual terms with identical bitvecs: " << endl;
          cout << qi_q_terms[i] << endl;
          cout << qi_q_terms[j] << endl;
          for(const auto & x : bits[i]){
            cout << x << ' ';
          }
          cout << endl;
          for(const auto & x : bits[j]){
            cout << x << ' ';
          }
          cout << endl;
          assert(0);
        }
        else{
          cout << "Equal terms at " << i << " and " << j << endl;
          cout << qi_q_terms[i] << endl;
          cout << qi_q_terms[j] << endl;
        }   
      }
    }
  }
*/



}
