#include <iostream>

#include <gmp.h>
#include <gmpxx.h>

#include "utilities.h"

using namespace std;

int main(int argc, char ** argv){
	if(argc != 4){
		cout << "Usage: ./cdiv a b mod" << endl;
		return 0;
	}
	mpz_class a(argv[1]);
	mpz_class b(argv[2]);
	mpz_class mod(argv[3]);
	mpz_class result = centered_quotient(a, b, mod);
	cout << result << endl;
	return 0;
}