#include <iostream>
#include <vector>
#include <cassert>

#include <gmp.h>
#include <gmpxx.h>

using std::vector;
using std::endl;
using std::cerr;


template<typename T>
mpz_class DWR(const T & num, const T & den){
	mpf_class num_mpf(num);
	mpf_class den_mpf(den);
	mpf_class div_result = num_mpf / den_mpf;
	mpf_class ceil, upper_ceil;
	mpf_ceil(ceil.get_mpf_t(), div_result.get_mpf_t());
	div_result += 0.5f;
	mpf_ceil(upper_ceil.get_mpf_t(), div_result.get_mpf_t());
	mpz_class result(ceil);
	if(ceil == upper_ceil){
		result--;
	}
	return result;
}

inline mpz_class cmod_simple(const mpz_class & a, const mpz_class & mod){
  if(mod <= 0){
      assert(0 && "Modulus should be >0!");
  }
  mpz_class rem = a;
  mpz_class bound; 
  if(rem >= 0){
    bound = mod/2;
    while(rem >= bound){
      rem -= mod;
    }
  }
  else{
    bound = -(mod/2);
    while(rem < bound){
      rem += mod;
    }
  }
  return rem;
}

/*
inline mpz_class centered_mod(const mpz_class & arg, const mpz_class & mod){
	if(mod <= 0){
		//Probably should throw some kind of error...TODO
		assert(0);
		return 0;
	}
	mpz_class remainder, bound;
	mpz_tdiv_r(remainder.get_mpz_t(), arg.get_mpz_t(), mod.get_mpz_t());
	mpz_fdiv_q_2exp(bound.get_mpz_t(), mod.get_mpz_t(), 1);
	if(remainder > 0){
		if(remainder >= bound){
			remainder -= mod;
		}
	}
	else{
		mpz_neg(bound.get_mpz_t(), bound.get_mpz_t());
		if(remainder < bound){
			remainder += mod;
		}
	}
	return remainder;
}


*/
inline mpz_class centered_mod(const mpz_class & arg, const mpz_class & mod, 
	bool lower_bound = false){
	if(mod <= 0){
		//Probably should throw some kind of error...TODO
		assert(0);
		return 0;
	}
	if(arg < 0){
		mpz_class neg;
		mpz_neg(neg.get_mpz_t(), arg.get_mpz_t());
		neg = centered_mod(neg, mod, true);
		mpz_neg(neg.get_mpz_t(), neg.get_mpz_t());
		return neg;
	}
	//At this point we know arg and mod are both > 0
	mpz_class remainder;
	mpz_fdiv_r(remainder.get_mpz_t(), 
		arg.get_mpz_t(), mod.get_mpz_t());
	//We now have a quotient for regular integer division, so the remainder would be in 
	mpz_class upper_bound;

	//Set upper_bound to mod/2
	//upper_bound = mod/2;
	mpz_tdiv_q_2exp(upper_bound.get_mpz_t(), mod.get_mpz_t(), 1);
	//If we're already in range, we don't have to do anything
	//If not, we then have to do 1 more reduction (don't actually need to do the calculation)
	
	if(lower_bound){
		if(remainder > upper_bound){
			remainder -= mod;
		}
	}
	else{
		if(remainder >= upper_bound){
			remainder -= mod;
		}
	}
	return remainder;	
}


//Old version - slow, probably because of float-based DWR.
template<typename T>
mpz_class centered_mod_DWR(const T & arg, const T & red){
	if(red <= 0){
		//Probably should throw some kind of error...TODO
		assert(0);
		return 0;
	}
	mpz_class upper_bound = red/2;
	mpz_class lower_bound = -(red/2);
	//If we're already in range, we don't have to do anything
	if((arg < upper_bound) && (arg >= lower_bound)){
		return arg;
	}
	mpz_class dwr = DWR(arg, red);
	mpz_class centered = arg - (dwr*red);
	return centered;	
}


template<typename T>
signed long centered_mod_si(const T & arg, const T & red){
	mpz_class result = centered_mod(arg, red);
	return mpz_get_si(result.get_mpz_t());
}

//Pure multiplication - no reduction
template<typename T>
vector<T> coeff_multiply(const vector<T> & a, const vector<T> & b){
	vector<T> result(std::min(a.size(), b.size()));
	for(size_t i = 0; i < result.size(); i++){
		result[i] = a[i] * b[i];
	}
	return result;
}

template<typename T>
vector<mpz_class> reduce(const vector<T> & vals, const vector<T> & moduli){
	vector<mpz_class> result(vals.size());
	for(size_t i = 0; i < result.size(); i++){
		result = centered_mod(vals[i], moduli[i]);
	}
	return result;
}

template<typename T>
vector<T> mult_and_reduce(const vector<T> & a, const vector<T> & b, const vector<T> & moduli){
	vector<T> multiplied = coeff_multiply(a, b);
	return reduce(multiplied, moduli);
}

template<typename T>
mpz_class mult_and_reduce(const T & a, const T & b, const T & modulus){
	T prod = a*b;
	return centered_mod(prod, modulus);
}

mpz_class mod_inv_regular(const mpz_class & a, const mpz_class & modulus){
  if(a == 0 || modulus <= 0){
		return mpz_class(0);
	}
  mpz_class g, s, t;
  mpz_gcdext(g.get_mpz_t(), s.get_mpz_t(), t.get_mpz_t(), a.get_mpz_t(), modulus.get_mpz_t());
  assert(g == 1 && "Arguments not coprime!");
  return s;
}

//Fails whenever a or b divide each other?
template<typename T>
mpz_class mod_inv_centered(const T & a, const T & modulus){
	if(a == 0 || modulus <= 0){
		return mpz_class(0);
	}
	mpz_class g, s, t, a_centered;
	a_centered = centered_mod(a, modulus);
	mpz_gcdext(g.get_mpz_t(), s.get_mpz_t(), t.get_mpz_t(),
	 a.get_mpz_t(), modulus.get_mpz_t());
	//DEBUG
	if(g!=1){
		cerr << "WARNING: " << a << " and " << modulus << " are not coprime!" << endl;
	}
	//DEBUG
	/*
	if(g == a || g == modulus){
		cerr << "WARNING: a and modulus are coprime" << endl;
	}
	*/
	//Check result
	mpz_class s_centered = centered_mod(s, modulus);
	mpz_class product = s_centered*a_centered;
	mpz_class reduced = centered_mod(product, modulus);

	//Could be stripped later
	if(reduced != 1){
		cerr << "ERROR: inverse calculation incorrect" << endl;
		cerr << "reduced product with inverse was " << reduced << endl;
		cerr << "a: " << a << endl;
		cerr << "modulus: " << modulus << endl;
		cerr << "s: " << s << endl;
		assert(0 && "ERROR: inverse calculation incorrect");
		return 0;
	}
	else{
		return s_centered;
	}

	assert(0 && "Bad code path");
	return 0;
}


template<typename T>
mpz_class reconstruct_primes(const T & x, const T & y, const T & mod1, 
	const T & mod2){
	T mod_product = mod1*mod2;
	return reconstruct_primes(x, y, mod1, mod2, mod_product);
}

//Only takes in two currently
template<typename T>
mpz_class reconstruct_primes(const T & x, const T & y, const T & mod1, 
	const T & mod2, const T & mod_product){
	mpz_class x_mcand_inv = mod_inv_centered(mod2, mod1);
	mpz_class y_mcand_inv = mod_inv_centered(mod1, mod2);
	if(x_mcand_inv == 0 || y_mcand_inv == 0){
		std::cerr << "ERROR with modular inverses" << std::endl;
		std::cerr << "x_mcand_inv: " << x_mcand_inv << std::endl;
		std::cerr << "y_mcand_inv: " << y_mcand_inv << std::endl;
		assert(0 && "ERROR with modular inverses");
		return 0;
	}
	T firstprod = mult_and_reduce(x, x_mcand_inv, mod1);
	T secondprod = mult_and_reduce(y, y_mcand_inv, mod2);
	firstprod *= mod1;
	secondprod *= mod2;
	firstprod += secondprod;
	mpz_class centered = centered_mod(firstprod, mod_product);
	return centered;
}

//Not correct-do not use!
template<typename T>
mpz_class centered_quotient(const T & a_in, const T & b, const T & modulus){
	//Assuming modulus > 0
	//Probably need other error checks in an actual application, e.g. check a,b in range
	if(b == 0){
		assert(0 && "Cannot divide by zero!");
	}
	if(b < 0){
		return -1 * centered_quotient(a_in, -1*b, modulus);
	}
	if(a_in < 0){

	}
	mpz_class upper_bound = modulus/2;
	mpz_class lower_bound = -1*upper_bound;
	mpz_class q = 0;
	mpz_class a = a_in;
	mpz_class b_abs = b;
	//Get a positive value for b
	if(b_abs < 0){
		b_abs *= -1;
	}
	if(a <= 0){
		while(a < lower_bound){
			a += b_abs;
			q++;
		}
		if(b > 0){
			q *= -1;
		}
	}
	else{
		while(a >= upper_bound){
			a -= b_abs;
			q++;
		}
		if(b < 0){
			q *= -1;
		}
	}
	return q;
}

template<typename T>
T product(const vector<T> & nums){
	T prod = 1;
	for(size_t i = 0; i < nums.size(); i++){
		prod *= nums[i];
	}
	return prod;
}

template<typename T>
mpz_class product_reduced(const vector<T> & nums, const T & mod){
	mpz_class prod = product(nums);
	return centered_mod(prod, mod);
}

template<typename T>
vector<mpz_class> inverses_reduced(const vector<T> & nums, const vector<T> & mods){
	vector<mpz_class> inv(nums.size());
	for(size_t i = 0; i < inv.size(); i++){
		inv[i] = mod_inv_centered(nums[i], mods[i]);
	}
	return inv;
}

template<typename T>
vector<mpz_class> inverses_reduced(const vector<T> & nums, const T & mod){
	vector<mpz_class> inv(nums.size());
	for(size_t i = 0; i < inv.size(); i++){
		inv[i] = mod_inv_centered(nums[i], mod);
	}
	return inv;
}

template<typename T>
vector<mpz_class> inverses_reduced_regular(const vector<T> & nums, const T & mod){
	vector<mpz_class> inv(nums.size());
	for(size_t i = 0; i < inv.size(); i++){
		mpz_class g, t;
		mpz_gcdext(g.get_mpz_t(), inv[i].get_mpz_t(), t.get_mpz_t(),
	 		nums[i].get_mpz_t(), mod.get_mpz_t());
		assert((nums[i] * inv[i]) % mod) == 1;
	}
	return inv;
}

template<typename T>
vector<T> inverses_reduced_regular_ui(const vector<T> & nums, const T & mod){
	vector<T> inv(nums.size());
	mpz_class b = mod;
	for(size_t i = 0; i < inv.size(); i++){
		mpz_class g, s, t, a;
		a = nums[i];
		mpz_gcdext(g.get_mpz_t(), s.get_mpz_t(), t.get_mpz_t(),
	 		a.get_mpz_t(), mod.get_mpz_t());
		inv[i] = mpz_get_ui(s.get_mpz_t());
		assert((nums[i] * inv[i]) % mod) == 1;
	}
	return inv;
}

template<typename T>
vector<mpz_class> inverses_reduced_regular(const vector<T> & nums, const vector<T> & mods){
	vector<mpz_class> inv(nums.size());
	for(size_t i = 0; i < inv.size(); i++){
		mpz_class g, t;
		mpz_gcdext(g.get_mpz_t(), inv[i].get_mpz_t(), t.get_mpz_t(),
	 		nums[i].get_mpz_t(), mods[i].get_mpz_t());
		if(inv[i] < 0){
			inv[i] += mods[i];
		}
		if((nums[i] * inv[i]) % mods[i] != 1){
			std::cerr << "nums " << nums[i] << std::endl;
			std::cerr << "inv " << inv[i] << std::endl;
			std::cerr << "mods " << mods[i] << std::endl;
			std::cerr << "product " << nums[i] * inv[i] << std::endl;
			std::cerr << "reduced product " << (nums[i] * inv[i]) % mods[i] << std::endl;
			assert((nums[i] * inv[i]) % mods[i] == 1);
		}
		
	}
	return inv;
}

//Returns {q/q_i} - no reduction yet!
//This version has more multiplications, but no modular inverses
template<typename T>
vector<T> CRT_mcands(const vector<T> & coprimes){
	vector<T> mcands(coprimes.size());
	for(size_t i = 0; i < mcands.size(); i++){
		mcands[i] = 1;
		for(size_t j = 0; j < mcands.size(); j++){
			if(j == i){
				continue;
			}
			mcands[i] *= coprimes[j];
		}
	}
	return mcands;
}

//Could easily be templated
mpz_class uint64_to_mpz(uint64_t * words, size_t count = 2){
	mpz_class result = words[--count];
	while(count--){
		result <<= 8*sizeof(uint64_t);
		result += words[count];
	}
	return result;
}

/*
mpz_class reconstruct_2exp(const T & x, const T & y, 
	unsigned int k){
	mpz_class x_modulus = 1;
	x_modulus = (x_modulus << 
	mpz_class x_reduced = centered_mod(x, )
}
*/
